! function (e, t) {
    if ("object" == typeof exports && "object" == typeof module) module.exports = t();
    else if ("function" == typeof define && define.amd) define([], t);
    else {
        var n = t();
        for (var r in n)("object" == typeof exports ? exports : e)[r] = n[r]
    }
}(window, function () {
    return function (e) {
        var t = {};

        function n(r) {
            if (t[r]) return t[r].exports;
            var o = t[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return e[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports
        }
        return n.m = e, n.c = t, n.d = function (e, t, r) {
            n.o(e, t) || Object.defineProperty(e, t, {
                enumerable: !0,
                get: r
            })
        }, n.r = function (e) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(e, "__esModule", {
                value: !0
            })
        }, n.t = function (e, t) {
            if (1 & t && (e = n(e)), 8 & t) return e;
            if (4 & t && "object" == typeof e && e && e.__esModule) return e;
            var r = Object.create(null);
            if (n.r(r), Object.defineProperty(r, "default", {
                    enumerable: !0,
                    value: e
                }), 2 & t && "string" != typeof e)
                for (var o in e) n.d(r, o, function (t) {
                    return e[t]
                }.bind(null, o));
            return r
        }, n.n = function (e) {
            var t = e && e.__esModule ? function () {
                return e.default
            } : function () {
                return e
            };
            return n.d(t, "a", t), t
        }, n.o = function (e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }, n.p = "/codebase/", n(n.s = 207)
    }({
        207: function (e, t) {
            gantt.locale = {
                date: {
                    month_full: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
                    month_short: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
                    day_full: ["อาทิตย์ ", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
                    day_short: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."]
                },
                labels: {
                    new_task: "เพิ่มกิจกรรม",
                    icon_save: "บันทึก",
                    icon_cancel: "ยกเลิก",
                    icon_details: "รายละเอียด",
                    icon_edit: "แก้ไข",
                    icon_delete: "ลบ",
                    confirm_closing: "",
                    confirm_deleting: "คุณต้องการลบกิจกรรมนี้ใช่หรือไม่?",
                    section_description: "รายละเอียด",
                    section_time: "ช่วงเวลา",
                    section_type: "ประภท",
                    column_wbs: "WBS",
                    column_text: "กิจกรรม",
                    column_start_date: "ช่วงเวลาเริ่มต้น",
                    column_duration: "ระยะเวลา",
                    column_add: "",
                    link: "Link",
                    confirm_link_deleting: "ข้อมูลจะถูกลบ",
                    link_start: " (เริ่มต้น)",
                    link_end: " (สิ้นสุด)",
                    type_task: "กิจกรรม",
                    type_project: "โครงการ",
                    type_milestone: "Milestone",
                    minutes: "นาที",
                    hours: "ชั่วโมง",
                    days: "วัน",
                    weeks: "สัปดาห์",
                    months: "เดือน",
                    years: "ปี",
                    message_ok: "ตกลง",
                    message_cancel: "ยกเลิก"
                }
            }
        }
    })
});
//# sourceMappingURL=locale.js.map