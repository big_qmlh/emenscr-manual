var keycloak = null;

const API_AUTH = 'https://emenscr.nesdc.go.th/auth';
const API_DOCUMENT_STABLE = 'https://emenscr.nesdc.go.th/api/stable/';
const API_DOCUMENT_ENDPOINT = API_DOCUMENT_STABLE + 'documents/';
const API_USER_ENDPOINT = API_DOCUMENT_STABLE + 'users/';
const API_ACTIONPLAN_ENDPOINT = API_DOCUMENT_STABLE + 'actionplans/';

var global_user_has_workflow = false;
var global_first_authen = true;
var global_emenscr_role = null;

const approver_inspector_list = [
    'moi-chief1-demo',
    'moi-chief1'
];

const path_shared = [
    'tools',
    'committee',
    'plan3',
    'actionplan',
    'planlevel3',
    'a',
    'user',
    'data',
    'dashboard',
    'files',
    'invisible_inspector',
    'internal',
    'covid19',
    'thaime',
    'thaime2',
    'public',
    'viewer',
    'm1'
];

const NS_COMMITTEE_USER = [
    'pornpipat.ben',
    'apirat.kong',
    'luechai.rudd',
    'maanat.wong',
    'chakthip.chai',
    'somsak.roo',
    'sanit.aks',
    'prapat.panya',
    'kalin.sar',
    'supant.mong',
    'chairat.tri',
    'payong.sri',
    'kan.trak',
    'chartsiri.soph',
    'tianchai.kir',
    'banthoon.lams',
    'wissanu.krea',
    'supachai.pan',
    'somkid.ja',
    'anupong.pao',
    'uttama.savana',
    'tosaporn.siri',
    'danucha.picha',
    'chatchai.bang',
]

$(document).ready(function () {
    console.log('V-2020-07-24');

    var _clientId = 'emenscr-nesdc';

    if (window.location.hostname == 'localhost') {
        _clientId = 'authdev-local';
    } else if (window.location.hostname == '203.146.107.90') {
        _clientId = 'emenscr-web-dev2';
    } else {
        _clientId = 'emenscr-nesdc';
    }

    $('#sidebar-menu-title-actionplan').hide();
    $('#sidebar-menu-list-actionplan').hide();

    keycloak = Keycloak({
        url: API_AUTH,
        realm: 'master',
        clientId: _clientId,
    });

    keycloak.init({
        onLoad: 'login-required',
        checkLoginIframe: false,
    }).success(function (authenticated) {
        if (!authenticated) {
            alert('not authenticated');
        } else {

            global_emenscr_role = emenscr_get_current_role(keycloak.tokenParsed);
            console.log('emenscr-role: ', global_emenscr_role);

            var current_pathname = window.location.pathname.split('/');

            if (current_pathname.length <= 2) {

                if (current_pathname[current_pathname.length - 1] == 'contents.html') {
                    load_content_onstart();
                    return;
                }

                if (global_emenscr_role == 'reporter') {
                    window.location = "reporter/main.html?" + keycloak.tokenParsed.preferred_username;
                } else if (global_emenscr_role == 'invisible_inspector') {
                    window.location = "approver/inspector.html?" + keycloak.tokenParsed.preferred_username;
                } else if (global_emenscr_role == 'approver') {
                    window.location = "approver/pending.html?" + keycloak.tokenParsed.preferred_username;
                } else if (global_emenscr_role == 'committee') {
                    if (NS_COMMITTEE_USER.includes(keycloak.tokenParsed.preferred_username)) {
                        window.location = "committee/main.html?project65=true&" + keycloak.tokenParsed.preferred_username;
                    } else {
                        window.location = "committee/summary.html?" + keycloak.tokenParsed.preferred_username;
                    }
                } else if (global_emenscr_role == 'loan-act-reporter') {
                    window.location = "thaime/main.html?" + keycloak.tokenParsed.preferred_username;
                } else {
                    window.location = "committee/summary.html?" + "role_error&" + keycloak.tokenParsed.preferred_username;
                }
            } else {

                if (current_pathname.length > 0 && global_emenscr_role != "invisible_inspector") {
                    if (!(path_shared.some(r => current_pathname.indexOf(r) >= 0))) {
                        if (!current_pathname.includes(global_emenscr_role)) {
                            window.location = "https://emenscr.nesdc.go.th/";
                        }
                    }
                }

                if (get_actionplan_role(keycloak.tokenParsed)) {
                    $('#sidebar-menu-title-actionplan').show();
                    $('#sidebar-menu-list-actionplan').show();
                    //$('#div_new_project_idea').hide();
                } else {
                    // Hide idea project menu
                    //$('#div_new_project_idea').hide();
                }

                _paq.push(['trackEvent', 'page_open', global_emenscr_role, keycloak.tokenParsed.preferred_username]);
                //ga('send', global_emenscr_role, 'page_open', 'username', keycloak.tokenParsed.preferred_username);

                console.log(keycloak.tokenParsed.groups)

                if (check_user_workflow(keycloak.tokenParsed)) {
                    // User has workflow path

                    //  Check extra condition
                    if (check_extra_condition(keycloak.tokenParsed)) {
                        console.log('global_user_has_workflow() :: check_extra_condition() -> False');
                        global_user_has_workflow = false;
                    } else {
                        console.log('global_user_has_workflow() -> True');
                        global_user_has_workflow = true;
                    }

                } else {
                    global_user_has_workflow = false;
                    console.log('global_user_has_workflow() -> False');

                }

                load_content_onstart();
                document_ready_load();
            }
        }
    }).error(function () {
        alert('failed to initialize');
    });

});

setInterval(function () {
    keycloak.updateToken();
}, 60000);

$('#nav_user_logout').click(function () {
    console.log('Log out')
    var redirectUri = 'https://emenscr.nesdc.go.th/';

    if (keycloak.tokenParsed.aud == "emenscr-web-dev") {
        redirectUri = 'https://203.146.107.90:9647';

    } else if (keycloak.tokenParsed.aud == "emenscr-web-dev2") {
        redirectUri = 'https://203.146.107.90:9647';

    }

    keycloak.logout({
        redirectUri: redirectUri
    });
});

function load_content_onstart() {
    document.getElementsByTagName("html")[0].style.visibility = "visible";

    // document_ready_load();
    update_user_navbar_info();
}

function update_user_navbar_info() {
    var _username_header = '';

    if (global_emenscr_role == 'reporter') {
        if (typeof keycloak.tokenParsed.level3 === 'undefined') {
            _username_header = keycloak.tokenParsed.preferred_username;
        } else {
            _username_header = keycloak.tokenParsed.level3 + ' (' + keycloak.tokenParsed.preferred_username + ')';
        }

        update_user_notification();

    } else if (global_emenscr_role == 'approver') {
        if (typeof keycloak.tokenParsed.position === 'undefined') {
            _username_header = keycloak.tokenParsed.preferred_username;
        } else {
            _username_header = keycloak.tokenParsed.position + ' (' + keycloak.tokenParsed.preferred_username + ')';
        }

        update_user_notification();

        update_actionplan_counter();

    } else {
        if (typeof keycloak.tokenParsed.given_name === 'undefined') {
            _username_header = keycloak.tokenParsed.preferred_username;
        } else {
            if (typeof keycloak.tokenParsed.title === 'undefined') {
                _username_header = keycloak.tokenParsed.given_name + ' ' + keycloak.tokenParsed.family_name + ' (' + keycloak.tokenParsed.preferred_username + ')';
            } else {
                _username_header = keycloak.tokenParsed.title + keycloak.tokenParsed.given_name + ' ' + keycloak.tokenParsed.family_name + ' (' + keycloak.tokenParsed.preferred_username + ')';

            }
        }
    }

    $('#nav_user_header_username').text(_username_header);
    $('#nav_user_info_username').text(keycloak.tokenParsed.preferred_username);
    $('#nav_user_info_level_3').text(keycloak.tokenParsed.level3);
    $('#nav_user_info_level_2').text(keycloak.tokenParsed.level2);
    $('#nav_user_info_level_1').text(keycloak.tokenParsed.level1);

    if (global_emenscr_role == 'approver') {
        var _approver_name = '';

        if (typeof keycloak.tokenParsed.title != 'undefined') {
            _approver_name += keycloak.tokenParsed.title
        }

        if (typeof keycloak.tokenParsed.given_name != 'undefined') {
            _approver_name += keycloak.tokenParsed.given_name
        }

        if (typeof keycloak.tokenParsed.family_name != 'undefined') {
            _approver_name += ' ' + keycloak.tokenParsed.family_name
        }

        $('#nav_user_info_level_3').text(_approver_name);

    } else {
        if (global_emenscr_role == 'committee' || global_emenscr_role == 'undefined') {
            $('#nav_admin_separator').css('display', 'none');
            $('#nav_public_document').css('display', 'none');
        }
    }

}

async function update_user_notification() {
    let status_count = await get_status_stats(keycloak.tokenParsed.preferred_username);

    if (status_count[0].reporterstats.length > 0) {
        // Reporter
        let count_draft = typeof status_count[0].reporterstats.find(item => item.status === 'เอกสารร่าง') != 'undefined' ? status_count[0].reporterstats.find(item => item.status === 'เอกสารร่าง').count : 0;
        let count_pending = typeof status_count[0].reporterstats.find(item => item.status === 'เอกสารรออนุมัติ') != 'undefined' ? status_count[0].reporterstats.find(item => item.status === 'เอกสารรออนุมัติ').count : 0;
        let count_reject = typeof status_count[0].reporterstats.find(item => item.status === 'เอกสารรอการแก้ไข') != 'undefined' ? status_count[0].reporterstats.find(item => item.status === 'เอกสารรอการแก้ไข').count : 0;

        //$('#reporter_count_draft').text(count_draft);
        $('#reporter_count_pending').text(count_pending);
        $('#reporter_count_reject').text(count_reject);

        if (count_reject > 0) {
            $('#nav_span_notification_bell').addClass('badge badge-danger noti-icon-badge');

            $(`
                <div class="dropdown-item noti-title">
                    <h5 id="nav_notification_header">การแจ้งเตือน (1)</h5>
                </div>
            `).appendTo('#nav_notification_dropdown_list');

            $(`
                <a href="/reporter/reject.html" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="mdi mdi-message"></i></div>
                    <p class="notify-details"><b>มีโครงการถูกส่งกลับมาแก้ไขจำนวน ${count_reject} รายการ</b></p>
                </a>
            `).appendTo('#nav_notification_dropdown_list');

        } else {
            $(`
                <a href=";" class="dropdown-item notify-item">
                    <div class="notify-icon bg-secondary"><i class="mdi mdi-message"></i></div>
                    <p class="notify-details"><b>ไม่มีการแจ้งเตือน</b></p>
                </a>
            `).appendTo('#nav_notification_dropdown_list');
        }

    } else if (status_count[0].approverstats.length > 0) {
        // Approver
        let count_pending = typeof status_count[0].approverstats.find(item => item.status === 'เอกสารรออนุมัติ') != 'undefined' ? status_count[0].approverstats.find(item => item.status === 'เอกสารรออนุมัติ').count : 0;

        $('#approver_pending_count').text(count_pending);

        if (count_pending > 0) {
            $('#nav_span_notification_bell').addClass('badge badge-danger noti-icon-badge');

            $(`
                    <div class="dropdown-item noti-title">
                        <h5 id="nav_notification_header">การแจ้งเตือน (1)</h5>
                    </div>
                `).appendTo('#nav_notification_dropdown_list');

            $(`
                    <a href="/reporter/reject.html" class="dropdown-item notify-item">
                        <div class="notify-icon bg-success"><i class="mdi mdi-message"></i></div>
                        <p class="notify-details"><b>มีโครงการรอท่านอนุมัติจำนวนทั้งสิ้น ${count_pending} รายการ</b></p>
                    </a>
                `).appendTo('#nav_notification_dropdown_list');

        } else {
            $(`
                    <a href=";" class="dropdown-item notify-item">
                        <div class="notify-icon bg-secondary"><i class="mdi mdi-message"></i></div>
                        <p class="notify-details"><b>ไม่มีการแจ้งเตือน</b></p>
                    </a>
                `).appendTo('#nav_notification_dropdown_list');
        }
    }

}

async function update_actionplan_counter() {
    let status_count = await get_actionplan_stats(keycloak.tokenParsed.preferred_username);
    
    $('#approver_actionplan_pending_count').text(status_count.length);
}

function check_workflow_status() {
    var _username = keycloak.tokenParsed.preferred_username;
    var _url = 'https://emenscr.nesdc.go.th/api/documents/workflows/raw/' + _username;

    const _headers = {
        apikey: 'oELEdPo9bCeecOCuLBptxZ9Hkkx5RwAD',
    }

    axios.get(_url, {
            headers: _headers,
            params: {}
        })
        .then(function (response) {

            if (response.data === null) {
                if (!check_user_workflow(keycloak.tokenParsed)) {
                    $('#message_popup_onstart').modal('hide');
                    $('#message_popup_reject_onstart').modal('hide');
                    $('#message_popup_enable_workflow_onstart').modal('hide');
                    $('#message_popup_plan3_onstart').modal('hide');

                    $('#message_popup_redirect_workflow').modal('show');
                }

            }
        })
        .catch(function (error) {
            console.log(error);
        });
}

function check_user_workflow(_kc) {
    if (typeof _kc.resource_access.emenscr != "undefined") {
        if (typeof _kc.groups[0] != "undefined") {
            if (_kc.groups[0].length > 0) {
                return true;
            }
        }

        return false;
    } else {
        return false;
    }
}

function check_emenscr_role(_kc, _role) {
    if (typeof _kc.resource_access.emenscr != "undefined") {
        if (_kc.resource_access.emenscr.roles.length > 0) {

            if (_kc.resource_access.emenscr.roles.includes(_role)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    } else {
        return false;
    }
}

function get_emenscr_role(_kc) {
    if (typeof _kc.resource_access.emenscr != "undefined") {
        return _kc.resource_access.emenscr.roles[0];
    } else {
        return 'reporter';
    }
}

function check_emenscr_client(_kc) {
    if (typeof _kc.resource_access.emenscr != "undefined") {
        return true;
    } else {
        return false;
    }
}

function emenscr_get_current_role(_kc) {
    /*
        return -> reporter||approver||committee
    */

    if (typeof _kc.resource_access.emenscr != "undefined") {
        if (_kc.resource_access.emenscr.roles.includes('reporter')) {
            return 'reporter';
        } else if (_kc.resource_access.emenscr.roles.includes('reporter_ts')) {
            return 'reporter';
        } else if (_kc.resource_access.emenscr.roles.includes('invisible_inspector')) {
            return 'invisible_inspector';
        } else if (_kc.resource_access.emenscr.roles.includes('approver')) {
            return 'approver';
        } else if (_kc.resource_access.emenscr.roles.includes('committee')) {
            return 'committee';
        } else if (_kc.resource_access.emenscr.roles.includes('loan-act-reporter')) {
            return 'loan-act-reporter';
        } else {
            return 'reporter';
        }
    } else if (typeof _kc.emenscr_role != "undefined") {
        if (_kc.emenscr_role == 'reporter') {
            return 'reporter';
        } else if (_kc.emenscr_role == 'reporter_ts') {
            return 'reporter';
        } else if (_kc.emenscr_role == 'invisible_inspector') {
            return 'invisible_inspector';
        } else if (_kc.emenscr_role == 'approver') {
            return 'approver';
        } else if (_kc.emenscr_role == 'committee') {
            return 'committee';
        } else if (_kc.emenscr_role == 'loan-act-reporter') {
            return 'loan-act-reporter';
        } else {
            return 'reporter';
        }
    } else {
        return 'reporter';
    }
}

function check_committee_authorize(_kc) {
    if (_kc.emenscr_role == 'committee') {
        return true;
    }

    return false;
}

function get_actionplan_role(_kc) {
    if (typeof _kc.resource_access.emenscr != "undefined") {
        if (_kc.resource_access.emenscr.roles.includes('upload-level3plan')) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function check_extra_condition(_kc) {
    if (typeof _kc.groups[0] != "undefined") {
        //mol-chief2        
        if ((_kc.groups[0].split('/').indexOf('mol-chief2')) >= 0) {
            return true;
        }

        return false;
    }

    return false;
}

function check_user_condition(_list) {
    if (_list.indexOf(keycloak.tokenParsed.preferred_username) >= 0) {
        return true;
    } else {
        return false;
    }
}

/* Ajax call for get document-status count. */

async function get_status_stats(_username) {
    const _token = get_keycloak_token();
    const _url = API_DOCUMENT_ENDPOINT + 'reports/statusstats';

    const _headers = {
        apikey: keycloak.tokenParsed.apikey,
        Authorization: _token,
    };

    const _params = {
        username: _username
    };

    return axios.get(
        _url, {
            headers: _headers,
            params: _params,
        }).then(response => response.data);
}

async function get_actionplan_stats() {

    const token = get_keycloak_token();

    const headers = {
        apikey: keycloak.tokenParsed.apikey,
        Authorization: token,
    }

    const params = {
        fields: '_id,status,approver'
    };

    return axios.get(
        API_ACTIONPLAN_ENDPOINT, {
            headers: headers,
            params: params,
        }).then(response => response.data.filter(x =>
            (x.status != 'deleted') && (x.approver == keycloak.tokenParsed.preferred_username) && (x.status == 'เอกสารรออนุมัติ')
        ));
}

/************ Helper Function ************/

function get_keycloak_token() {
    keycloak.updateToken();

    return 'Bearer ' + keycloak.token;
}

function get_url_parameters(_url) {
    let regex = /[?&]([^=#]+)=([^&#]*)/g,
        url = _url,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }

    return params;
}

/****** Shared event ********/
$("#btn_modal_new_project").click(function () {
    $('#menu_create_project').modal('show');
});