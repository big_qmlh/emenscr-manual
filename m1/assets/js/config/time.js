/* v1.2020.09.29.1 */


const CURRECT_FISCAL_YEAR = 2564;
const CURRECT_FISCAL_QUARTER = 2;

const CURRECT_PROGRESS_FISCAL_YEAR = 2564;
const CURRECT_PROGRESS_FISCAL_QUARTER = 1;

/*
2563_Q1 = '2562-10-01';
2563_Q2 = '2563-01-01';
2563_Q3 = '2563-04-01';
2563_Q4 = '2563-07-01';

2563_Q1 = '2562-12-01';
2563_Q2 = '2563-03-01';
2563_Q3 = '2563-06-01';
2563_Q4 = '2563-09-01';
*/

var progress_status_obj = {
    '2563_1': {
        fy: 2563,
        fq: 1,
        status: false,
    },
    '2563_2': {
        fy: 2563,
        fq: 2,
        status: false,
    },
    '2563_3': {
        fy: 2563,
        fq: 3,
        status: false,
    },
    '2563_4': {
        fy: 2563,
        fq: 4,
        status: false,
    },
    '2564_1': {
        fy: 2564,
        fq: 1,
        status: false,
    }
}

var progress_report_fyq = ['2562_3', '2562_4', '2563_1', '2563_2', '2563_3', '2563_4', '2564_1'];


function get_boundary_fyq(_fy, _fq) {
    let fq_start = ['10', '01', '04', '07'];
    let fq_end = ['12', '03', '06', '09'];

    let fy = _fq === 1 ? _fy - 1 : _fy;

    return {
        fq_start: String(fy) + '-' + fq_start[_fq - 1] + '-' + '01',
        fq_end: String(fy) + '-' + fq_end[_fq - 1] + '-' + '01'
    }
}