/* v1.2020.07.22.1 */

var page_status = 'create';
var document_id = null;
var user_id = null;
var document_code_th = '';

var is_project_idea = false;
var is_project_idea_passed = false;
var is_project_idea_duplicated = false;
var project_idea_id = '';
var project_idea_code_th = '';

var is_value_chain = true;

var page_open_max = 1;
var current_page = 1;

var fiscal_year_g = 0;
var fiscal_quarter_g = 0;
var fiscal_change = false;

var tab6_part1_textbox_prefix = "นาย";
var tab6_part1_textbox_prefix_director = "นาย";
var tab6_part1_textbox_prefix_coordinator = "นาย";

var global_project_year_state = [];
var global_submit_form_check = false;

var section_offset_id_select = 'section1';
var section_offset_top_before = 0;

var g_national_strategic_goals = -1;
var g_national_strategy_direct = -1;
var g_national_strategy_secondary = -1;
var g_governmentpolicies_check = -1

var g_homeland_policy_checked = [];
var g_homeland_policy_progress = [];

var g_new_progress = true;
var g_new_progress_id = -1;