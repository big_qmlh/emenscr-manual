/* v1.2020.09.29.1 */

const global_fiscal_year_g = 2564;
const global_fiscal_quarter_g = 1;

const global_googlemap_api_key = 'AIzaSyBXhqKG8YRDqZP4oUO8m3OASA114nDRD60';
const global_googlemap_static_api_key = 'AIzaSyDC7WIBgu7HZo68LQSnEvkCaBdgWZVxcek';

// Date formate (B.E.): YYYY-MM-DD
var start_date_select = '2563-10-01';

var date_start_cutoff = '2563-10-01';
var date_end_cutoff = '2563-09-30';

var update_progress_only_flag = true;

function load_google_map() {
    var script = document.createElement('script');

    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + global_googlemap_api_key + '&libraries=places&callback=initAutocomplete';
    document.body.appendChild(script);
}