/* v1.2019.10.30.1 */

var count_tree_load = 0;
var gantt_data_onload = '';

var json_full_source = [];

function document_ready_load() {
    inti_treeview_all(true);
    document.getElementsByTagName("html")[0].style.visibility = "visible";
}

function rander_document(_json_form_data) {
    var document_json = [];

    document_json = _json_form_data;

    json_full_source = _json_form_data;

    console.log(document_json);

    document_code_th = document_json.code_th;

    if (typeof document_json.value_chain_flag == 'undefined') {
        is_value_chain = false;

        $("#div_xyz_value_chain").hide();
        $("#div_xyz_factor").hide();
    }

    update_rander_tab_1(document_json);
}

function update_rander_tab_1(_json_form) {
    var _json_form_tag_document = _json_form;

    if (typeof _json_form.fiscal_year !== 'undefined' || typeof _json_form.fiscal_quarter !== 'undefined') {
        fiscal_year_g = _json_form.fiscal_year;
        fiscal_quarter_g = _json_form.fiscal_quarter;
    }

    g_national_strategic_goals = _json_form_tag_document.national_strategic_goals;
    g_national_strategy_direct = _json_form_tag_document.national_strategy_direct;
    g_national_strategy_secondary = _json_form_tag_document.national_strategy_secondary;
    g_governmentpolicies_check = _json_form_tag_document.governmentpolicies_check;

    $('#tab1_part1_textbox_project_name').val(_json_form_tag_document.name);

    check_checkbox(_json_form_tag_document.government_policy_priority_2019, 'governmentpolicies_checkbox_');

    try {
        initial_tree_onstart_main('strategy', _json_form_tag_document.national_strategy_direct);
    } catch (ex) {
        //console.log(ex);
    }

    try {
        initial_tree_onstart_option('strategy', _json_form_tag_document.national_strategy_secondary);
    } catch (ex) {
        //console.log(ex);
    }

    try {
        initial_tree_onstart_main('reform', _json_form_tag_document.national_reform_direct);
    } catch (ex) {
        //console.log(ex);
    }

    try {
        initial_tree_onstart_option('reform', _json_form_tag_document.national_reform_secondary);
    } catch (ex) {
        //console.log(ex);
    }

    try {
        initial_tree_onstart_main('plan12', _json_form_tag_document.plan12);
    } catch (ex) {
        //console.log(ex);
    }

    try {
        initial_tree_onstart_main('homeland', _json_form_tag_document.national_security_plan_2020);
    } catch (ex) {
        //console.log(ex);
    }

    try {
        initial_tree_onstart_main('governmentpolicies', _json_form_tag_document.government_policy_main_2019);
    } catch (ex) {
        //console.log(ex);
    }

    fill_textbox_dynamic_group('[name="tab1_ministry_plan_textbox_dynamic"]', _json_form_tag_document.ministry_plan);
    fill_textbox_dynamic_group('[name="tab1_related_laws_textbox_dynamic"]', _json_form_tag_document.related_laws);
    fill_textbox_dynamic_group('[name="tab1_related_government_ministers_textbox_dynamic"]', _json_form_tag_document.related_government_ministers);

    load_xyz_direct(_json_form_tag_document.masterplan_direct);
    load_xyz_secondary(_json_form_tag_document.masterplan_secondary);

    g_homeland_policy_checked = _json_form_tag_document.national_security_plan;
}

function load_xyz_direct(_xyz) {
    if (typeof _xyz === 'undefined') {
        return;
    }

    if (typeof _xyz.id !== 'undefined') {
        xyz_direct = _xyz;

        $('#xyz_table_primary_topic').text(get_xyz_text_byID(xyz_direct.y1_direction));
        $('#btn_add_xyz_direct').hide();
        $('#xyz_table_primary').show();

        auto_check_tree_strategies_direct([xyz_direct.z_3digits]);

        $('#div_info_strategies').hide();
        $('#div_block_strategies_direct').show();
        $('#block_xyz_option').show();

        $('#nav_pills_tab_header_strategies').removeClass('nav_pills_tab_disable');
        $('#nav_pills_tab_header_strategies').attr('data-toggle', 'tab');
        $('#nav_pills_tab_header_strategies').attr('title', '');
    }
}

function load_xyz_secondary(_xyz) {
    if (typeof _xyz === 'undefined') {
        return;
    }

    if (_xyz.length == 0) {
        return;
    }

    xyz_secondary_arr = _xyz;
    xyz_secondary_arr_temp = _xyz;

    $('#div_info_strategies').hide();
    $('#div_block_strategies_option').show();

    for (var i = 0; i < xyz_secondary_arr.length; i++) {
        var _row = '<tr><td>';
        _row += '<strong>แนวทางการพัฒนารอง:&nbsp;</strong>' + get_xyz_text_byID(xyz_secondary_arr[i].y1_direction);
        _row += '</td><td class="table_td-center">';
        _row += '<a data-toggle="tooltip" title="แสดงข้อมูลแนวทางการพัฒนารอง" onclick="xyz_preview_popup(\'secondary\', \'' + xyz_secondary_arr[i].id + '\')" ';
        _row += 'class="btn btn-link btn-lg icon-blocks row-padding-rl-no">';
        _row += '<span class="glyphicon glyphicon-file icon-view"> </span></a>';
        _row += '<a data-toggle="tooltip" id="xyz_secondary_remove_' + xyz_secondary_arr[i].id + '" onclick="confirm_remove_xyz(\'secondary\', \'' + xyz_secondary_arr[i].id + '\')" title="ลบแนวทางการพัฒนารอง" class="btn btn-link btn-lg icon-blocks row-padding-rl-no">';
        _row += '<span class="glyphicon glyphicon-trash icon-danger"></span></a>';

        $('#xyz_table_secondary > tbody:last-child').append(_row);
    }

    var _temp_z_3digits = [];
    var _temp_z_goal = [];

    for (var i = 0; i < xyz_secondary_arr.length; i++) {
        _temp_z_3digits.push(xyz_secondary_arr[i].z_3digits);
        _temp_z_goal.push(xyz_secondary_arr[i].z_goal);
    }

    auto_check_tree_strategies_secondary(_temp_z_3digits);

    $('#xyz_table_group_secondary').show();

}

function fill_textbox_dynamic_group(_name_select, _json_list) {
    if (typeof _json_list === 'undefined') {
        return;
    }

    for (var i = 0; i < _json_list.length; i++) {
        if (i == 0) {
            $(_name_select).find(".btn-add").closest('.form-group').find('input').val(_json_list[i]);
        } else {
            add_form_textbox_group(_name_select, _json_list[i]);
        }
    }

}

function add_form_textbox_group(_name_select, _text) {
    var $formGroup = $(_name_select).find(".btn-add").closest('.form-group');
    var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
    var $formGroupClone = $formGroup.clone();

    $(_name_select).find(".btn-add")
        .toggleClass('btn-success btn-add btn-danger btn-remove')
        .html('<span class="glyphicon glyphicon-minus"></span>&nbsp;&nbsp;ลบ');

    $formGroupClone.find('input').val(_text);
    $formGroupClone.find('.concept').text('Phone');
    $formGroupClone.insertAfter($formGroup);

    var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
    if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
        $lastFormGroupLast.find('.btn-add').attr('disabled', true);
    }
}

var countFormGroup = function ($form) {
    return $form.find('.form-group').length;
};

function initial_tree_onstart_main(_type, _tree_selected) {
    if (typeof _tree_selected === 'undefined') {
        return;
    }

    if (_tree_selected.length == 0) {
        return;
    }

    if (_type == 'strategy') {
        $("#btn_treeview_trategies_planreform").text(get_category_strategies(_tree_selected[0]));
        initial_tree_strategies_main(_tree_selected);

    } else if (_type == 'reform') {
        $("#btn_treeview_planreform").text(get_category_reform(_tree_selected[0]));
        initial_tree_reform_main(_tree_selected);

    } else if (_type == 'plan12') {
        for (var i = 0; i < _tree_selected.length; i++) {
            tree_plan12_expand_selected(_tree_selected[i]);

        }

    } else if (_type == 'homeland') {
        for (var i = 0; i < _tree_selected.length; i++) {
            tree_homeland_policy_expand_selected(_tree_selected[i]);
        }

    } else if (_type == 'governmentpolicies') {
        for (var i = 0; i < _tree_selected.length; i++) {
            tree_governmentpolicies_expand_selected(_tree_selected[i]);
        }
    }

}

function initial_tree_onstart_option(_type, _tree_selected) {
    if (typeof _tree_selected === 'undefined') {
        return;
    }

    if (_tree_selected.length == 0) {
        return;
    }

    if (_type == 'strategy') {
        for (var i = 0; i < _tree_selected.length; i++) {
            tree_strategies_expand_selected('option', _tree_selected[i]);
        }

    } else if (_type == 'reform') {
        for (var i = 0; i < _tree_selected.length; i++) {
            tree_reform_expand_selected('option', _tree_selected[i]);
        }
    }
}

function check_checkbox(_check_index, _check_id_prefix) {
    if (typeof _check_index === 'undefined') {
        return;
    }

    var _id = null;

    for (i = 0; i < _check_index.length; i++) {
        _id = '#' + _check_id_prefix + (parseInt(_check_index[i]) + 1);
        $(_id).prop('checked', true);
    }
}

function initial_tree_strategies_main(_tree_selected) {
    $("#space_planreform_main_strategies_dynamic").hide();
    $("#space_planreform_main_strategies_loading").show();

    var dropdown_treeview_strategies_value = get_filename_strategies(_tree_selected[0]);
    var dropdown_treeview_strategies_text = get_category_strategies(_tree_selected[0]);

    if (tree_strategies_main_dynamic != null) {
        tree_strategies_main_dynamic.destroy();
        tree_strategies_main_dynamic = null;
    }

    var json_file_name = 'assets/treeview_jsons/v2/strategies/' + dropdown_treeview_strategies_value;

    tree_strategies_main_dynamic = $('#treeview_planreform_main_strategies_dynamic').tree({
        dataSource: json_file_name,
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {
            $("#space_planreform_main_strategies_loading").hide();
            $("#space_planreform_main_strategies_dynamic").show();

            var strategies_main_header_text = 'รายละเอียด' + dropdown_treeview_strategies_text;
            $("#header_planreform_main_strategies_dynamic").text(strategies_main_header_text);

            for (var i = 0; i < _tree_selected.length; i++) {
                tree_strategies_expand_selected('main', _tree_selected[i]);
            }
        }
    });

}

function initial_tree_reform_main(_tree_selected) {
    $("#space_planreform_main_dynamic").hide();
    $("#space_planreform_main_loading").show();

    var dropdown_treeview_planreform_value = get_filename_reform(_tree_selected[0]);
    var dropdown_treeview_planreform_text = get_category_reform(_tree_selected[0]);

    if (tree_planreform_main_dynamic != null) {
        tree_planreform_main_dynamic.destroy();
        tree_planreform_main_dynamic = null;
    }

    var json_file_name = 'assets/treeview_jsons/v2/planreform11/' + dropdown_treeview_planreform_value;

    tree_planreform_main_dynamic = $('#treeview_planreform_main_dynamic').tree({
        dataSource: json_file_name,
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {
            $("#space_planreform_main_loading").hide();
            $("#space_planreform_main_dynamic").show();
            var planreform_main_header_text = 'รายละเอียด' + dropdown_treeview_planreform_text;
            $("#header_planreform_main_dynamic").text(planreform_main_header_text);

            for (var i = 0; i < _tree_selected.length; i++) {
                tree_reform_expand_selected('main', _tree_selected[i]);
            }

            if (_tree_selected.length > 0) {
                tree_planreform_option.disable(tree_planreform_option.getNodeById(get_reform_root_id(dropdown_treeview_planreform_value)));
                $('#block_reform_option').show();
            }

        }
    });
}

function tree_reform_expand_selected(_type, _check_index) {
    var _id_arr = [];
    var _temp_id = '';

    if (_check_index.length < 3) {
        return;
    }

    _id_arr.push(_check_index.substring(0, 3));

    for (i = 5; i <= _check_index.length; i += 2) {
        _id_arr.push(_check_index.substring(i - 2, i));
    }

    for (i = 0; i < _id_arr.length; i++) {
        _temp_id = _temp_id + _id_arr[i];

        if (_type == 'main') {
            tree_planreform_main_dynamic.expand(tree_planreform_main_dynamic.getNodeById(_temp_id), false);
        } else if (_type == 'option') {
            tree_planreform_option.expand(tree_planreform_option.getNodeById(_temp_id), false);
        }
    }

    if (_type == 'main') {
        tree_planreform_main_dynamic.check(tree_planreform_main_dynamic.getNodeById(_check_index));
    } else if (_type == 'option') {
        tree_planreform_option.check(tree_planreform_option.getNodeById(_check_index));
    }
}

function tree_plan12_expand_selected(_check_index) {
    var _id_arr = [];
    var _temp_id = '';

    if (_check_index.length < 3) {
        return;
    }

    _id_arr.push(_check_index.substring(0, 3));

    for (i = 5; i <= _check_index.length; i += 2) {
        _id_arr.push(_check_index.substring(i - 2, i));
    }

    for (i = 0; i < _id_arr.length; i++) {
        _temp_id = _temp_id + _id_arr[i];
        tree_plan12.expand(tree_plan12.getNodeById(_temp_id), false);
    }

    tree_plan12.check(tree_plan12.getNodeById(_check_index));

}

function tree_homeland_policy_expand_selected(_check_index) {
    var _id_arr = [];
    var _temp_id = '';

    if (_check_index.length < 3) {
        return;
    }

    _id_arr.push(_check_index.substring(0, 4));

    for (i = 6; i <= _check_index.length; i += 2) {
        _id_arr.push(_check_index.substring(i - 2, i));
    }

    for (i = 0; i < _id_arr.length; i++) {
        _temp_id = _temp_id + _id_arr[i];
        tree_homeland_policy.expand(tree_homeland_policy.getNodeById(_temp_id), false);
    }

    tree_homeland_policy.check(tree_homeland_policy.getNodeById(_check_index));

}

function tree_governmentpolicies_expand_selected(_check_index) {
    var _id_arr = [];

    tree_government_policy.check(tree_government_policy.getNodeById(_check_index));

    for (i = 3; i <= _check_index.length; i += 2) {
        _id_arr.push(_check_index.substring(0, i));
    }

    for (i = 0; i < _id_arr.length; i++) {
        _temp_id = _id_arr[i];
        tree_government_policy.expand(tree_government_policy.getNodeById(_temp_id), false);
    }
}

function get_category_reform(_node_id) {
    var sub_node_id = _node_id.substring(0, 1);

    if (sub_node_id == 'A') {
        return 'แผนการปฏิรูปประเทศด้านการป้องกันและปราบปรามการทุจริตและประพฤติมิชอบ';
    } else if (sub_node_id == 'B') {
        return 'แผนการปฏิรูปประเทศด้านเศรษฐกิจ';
    } else if (sub_node_id == 'C') {
        return 'แผนการปฏิรูปประเทศด้านทรัพยากรธรรมชาติและสิ่งแวดล้อม';
    } else if (sub_node_id == 'D') {
        return 'แผนการปฏิรูปประเทศด้านพลังงาน';
    } else if (sub_node_id == 'E') {
        return 'แผนการปฏิรูปประเทศด้านการบริหารราชการแผ่นดิน';
    } else if (sub_node_id == 'F') {
        return 'แผนการปฏิรูปประเทศด้านสาธารณสุข';
    } else if (sub_node_id == 'G') {
        return 'แผนการปฏิรูปประเทศด้านกระบวนการยุติธรรม';
    } else if (sub_node_id == 'H') {
        return 'แผนการปฏิรูปประเทศด้านกฎหมาย';
    } else if (sub_node_id == 'I') {
        return 'แผนการปฏิรูปประเทศด้านสื่อสารมวลชน เทคโนโลยีสารสนเทศ';
    } else if (sub_node_id == 'J') {
        return 'แผนการปฏิรูปประเทศด้านการเมือง';
    } else if (sub_node_id == 'K') {
        return 'แผนการปฏิรูปประเทศด้านสังคม';
    } else if (sub_node_id == 'M') {
        return 'แผนการปฏิรูปประเทศด้านการศึกษา';
    } else {
        return '';
    }
}

function get_filename_reform(_node_id) {
    var sub_node_id = _node_id.substring(0, 1);

    if (sub_node_id == 'A') {
        return 'anticorruption.json';
    } else if (sub_node_id == 'B') {
        return 'economic.json';
    } else if (sub_node_id == 'C') {
        return 'ecosystem.json';
    } else if (sub_node_id == 'D') {
        return 'energy.json';
    } else if (sub_node_id == 'E') {
        return 'govadmin.json';
    } else if (sub_node_id == 'F') {
        return 'healthcare.json';
    } else if (sub_node_id == 'G') {
        return 'justice.json';
    } else if (sub_node_id == 'H') {
        return 'law.json';
    } else if (sub_node_id == 'I') {
        return 'media.json';
    } else if (sub_node_id == 'J') {
        return 'politics.json';
    } else if (sub_node_id == 'K') {
        return 'social.json';
    } else if (sub_node_id == 'M') {
        return 'education.json';
    } else {
        return '';
    }
}

function load_document(_document_id) {
    const _url = API_DOCUMENT_ENDPOINT + _document_id;
    const _token = get_keycloak_token();

    const _headers = {
        apikey: keycloak.tokenParsed.apikey,
        Authorization: _token,
    }

    var _params = {
        username: keycloak.tokenParsed.preferred_username
    };

    axios.get(_url, {
            headers: _headers,
            params: _params
        })
        .then(function (response) {
            rander_document(response.data);
        })
        .catch(function (error) {
            console.log(error);
            alert("ไม่สามารถใช้งานได้ชั่วคราว กรุณาเข้าใช้งานภายหลัง", error);

            return [];
        });
}