/* v1.2019.10.30.1 */

var tree_masterplan = null;
var tree_gold_y1 = null;
var tree_gold_y2 = null;
var tree_gold_z = null;
var tree_target_z = null;
var tree_plan_z = null;

var xyz_state = 'direct';
var xyz_state_remove = 'direct';

var xyz_template = [];
var xyz_direct = {};

$(document).ready(function () {
    $('#xyz_table_primary').hide();

    //$('#main_modal_select_masterplan').modal('show');
    $('.nav-tabs > li a[title]').tooltip();

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        // if ($target.parent().hasClass('disabled')) {
        //     return false;
        // }
    });

    $('#main_modal_select_masterplan').on('hidden.bs.modal', function () {
        $('#tab_step_1').removeClass('disabled');
        $('#a_tab_step_1').click();

        tree_masterplan.reload();
    });

    $('#tab_step_7').removeClass('disabled');
    $('#a_tab_step_7').click();

    init_xyz();
    //init_tab2();
    //init_tab3();
    //init_tab4();
    //init_tab5();
    //init_tab6();
    init_tab7();

});

$("#btn_add_xyz_direct").on("click", function (e) {
    xyz_state = 'direct';
    init_xyz();
    $('#main_modal_select_masterplan').modal('show');
});

var _xyz_secondary_count = 0;

$("#btn_add_masterplan_second").on("click", function (e) {
    //$('#main_modal_select_masterplan').modal('show');

    //var index = $("#xyz_table_secondary table tbody tr:last-child").index();

    //console.log(index);

    //console.log($("#xyz_table_secondary table tbody tr:last-child"))

    var _row = '<tr><td>';
    _row += '<strong>แนวทางการพัฒนารอง:&nbsp;</strong>';
    _row += '</td><td class="table_td-center">';
    _row += '<a data-toggle="tooltip" title="แสดงข้อมูลโครงการ" class="btn btn-link btn-lg icon-blocks row-padding-rl-no">';
    _row += '<span class="glyphicon glyphicon-file icon-view"> </span></a>';
    _row += '<a id="btn_xyz_table_secondary_remove_' + _xyz_secondary_count + '" data-toggle="tooltip" title="แสดงข้อมูลโครงการ" class="btn_xyz_table_secondary_remove_ btn btn-link btn-lg icon-blocks row-padding-rl-no">';
    _row += '<span class="glyphicon glyphicon-trash icon-danger"></span></a></td></tr>';

    $('#xyz_table_secondary > tbody:last-child').append(_row);

    _xyz_secondary_count++;

});


$(document).on("click", '[id^=btn_xyz_table_secondary_remove_]', function () {
    $(this).parents("tr").remove();
});

$("#btn_xyz_confirm_delete").on("click", function (e) {
    if (xyz_state_remove == 'direct') {
        xyz_template = {};
        xyz_direct = {};

        $('#xyz_table_primary').hide();
        $('#xyz_confirm_delete').modal('hide');
    }
});

$("#btn_masterplan_next_1").on("click", function (e) {
    $('#tab_step_2').removeClass('disabled');
    $('#a_tab_step_2').click();

    console.log(xyz_template);

    console.log('STEP 1: แผนแม่บทภายใต้ยุทธศาสตร์ชาติที่สอดคล้องกับโครงการ');
    console.log(xyz_template.masterplan + '\t' + get_xyz_text_byID(xyz_template.masterplan));
    console.log('----------------------------------------------------------------------');
    init_tab2();

});

$("#btn_masterplan_next_2").on("click", function (e) {
    $('#tab_step_3').removeClass('disabled');
    $('#a_tab_step_3').click();

    console.log('STEP 2.1: ป้าหมายของแผนย่อย Y1');
    console.log(xyz_template.gold_y1 + '\t' + get_xyz_text_byID(xyz_template.gold_y1));
    console.log('----------------------------------------------------------------------');

    console.log('STEP 2.2: เป้าหมายของแผนแม่บทประเด็น Y2');
    console.log(xyz_template.gold_y2 + '\t' + get_xyz_text_byID(xyz_template.gold_y2));
    console.log('----------------------------------------------------------------------');

    init_tab3();
});

$("#btn_masterplan_next_3").on("click", function (e) {
    $('#tab_step_4').removeClass('disabled');
    $('#a_tab_step_4').click();

    init_tab4();
});

$("#btn_masterplan_next_4").on("click", function (e) {
    xyz_template.report_z_consistent = $('#textbox_z_consistent').val();
    console.log(xyz_template);

    $('#tab_step_5').removeClass('disabled');
    $('#a_tab_step_5').click();

    init_tab5();
});

$("#btn_masterplan_next_5").on("click", function (e) {
    xyz_template.report_gold_y2.consistent = $('#textbox_gold_y2_consistent').val();

    var _temp_y2_indicators = [];

    $('[id^=textbox_gold_y2_contribution_]').each(function () {
        _temp_y2_indicators.push({
            "id": $(this).attr('id').split('_')[4],
            "contribution": $(this).val()
        });
    });

    xyz_template.report_gold_y2.indicators = _temp_y2_indicators;

    $('#tab_step_6').removeClass('disabled');
    $('#a_tab_step_6').click();

    init_tab6();
});

$("#btn_masterplan_next_6").on("click", function (e) {
    xyz_template.report_gold_y1.consistent = $('#textbox_gold_y1_consistent').val();

    var _temp_y1_indicators = [];

    $('[id^=textbox_gold_y1_contribution_]').each(function () {
        _temp_y1_indicators.push({
            "id": $(this).attr('id').split('_')[4],
            "contribution": $(this).val()
        });
    });

    xyz_template.report_gold_y1.indicators = _temp_y1_indicators;

    console.log(xyz_template);

    $('#tab_step_7').removeClass('disabled');
    $('#a_tab_step_7').click();
});

$("#btn_masterplan_back_2").on("click", function (e) {
    $('#tab_step_1').removeClass('disabled');
    $('#a_tab_step_1').click();
});

$("#btn_masterplan_back_3").on("click", function (e) {
    $('#tab_step_2').removeClass('disabled');
    $('#a_tab_step_2').click();
});

$("#btn_masterplan_back_4").on("click", function (e) {
    $('#tab_step_3').removeClass('disabled');
    $('#a_tab_step_3').click();
});

$("#btn_masterplan_back_5").on("click", function (e) {
    $('#tab_step_4').removeClass('disabled');
    $('#a_tab_step_4').click();
});

$("#btn_masterplan_back_6").on("click", function (e) {
    $('#tab_step_5').removeClass('disabled');
    $('#a_tab_step_5').click();
});

$("#btn_masterplan_back_7").on("click", function (e) {
    $('#tab_step_6').removeClass('disabled');
    $('#a_tab_step_6').click();
});

$("#btn_masterplan_save").on("click", function (e) {
    if (xyz_state == 'direct') {
        xyz_template.id = get_xyz_mid(xyz_template);

        xyz_direct = xyz_template;

        $('#xyz_table_primary_topic').text(get_xyz_text_byID(xyz_direct.masterplan));
        $('#xyz_table_primary').show();
        //console.log(xyz_direct);
    }

    $('#main_modal_select_masterplan').modal('hide');

});

$("#btn_xyz_primary_preview").on("click", function (e) {

});

function init_xyz() {
    json_directY1_goldY1 = load_directY1_goldY1();
    json_goldY1_goldY2 = load_goalY1_goalY2();

    json_goalY2_goalZ = load_goalY2_goalZ();
    json_goalZ_Z3digits = load_goalZ_Z3digits();
    json_directionY1_Z3digits = load_directionY1_Z3digits();

    json_value_goldY1_goldY2 = load_value_goldY1_goldY2();

    xyz_template1 = {
        "masterplan": "",
        "gold_y1": "",
        "gold_y2": "",
        "gold_z": "",
        "z_3digits": "",
        "report_z_consistent": "",
        "report_gold_y1": {
            "consistent": "",
            "indicators": []
        },
        "report_gold_y2": {
            "consistent": "",
            "indicators": []
        }
    };

    xyz_template = {
        "id": "",
        "masterplan": "23010001",
        "gold_y1": "190101",
        "gold_y2": "110001",
        "gold_z": "Y2010102",
        "z_3digits": "Y201030101",
        "report_z_consistent": "ZZZzzzzzzz",
        "report_gold_y1": {
            "consistent": "report_gold_y1",
            "indicators": [{
                    "id": "19010101",
                    "contribution": "10"
                },
                {
                    "id": "19010102",
                    "contribution": "20"
                }
            ]
        },
        "report_gold_y2": {
            "consistent": "report_gold_y2",
            "indicators": [{
                "id": "11000101",
                "contribution": "30"
            }]
        }
    };

    xyz_template_multiple = {
        "secondary": [{
                "id": "aaa",
                "masterplan": "แผนแม่บทภายใต้ยุทธศาสตร์ชาติที่สอดคล้องกับโครงการ",
                "gold_y1": "เป้าหมายของแผนย่อย",
                "gold_y2": "ป้าหมายของแผนแม่บทประเด็น",
                "gold_z": "เป้าหมายของยุทธศาสตร์ชาติ",
                "z_3digits": "ประเด็นของยุทธศาสตร์ชาติ",
                "z_consistent": "อธิบายความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก",
                "report_gold_y1": {
                    "consistent": "ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย",
                    "indicators": [{
                            "id": "y11111",
                            "contribution": "10"
                        },
                        {
                            "id": "y12222",
                            "contribution": "20"
                        },
                        {
                            "id": "y13333",
                            "contribution": "30"
                        }
                    ]
                },
                "report_gold_y2": {
                    "consistent": "ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย",
                    "indicators": [{
                        "id": "y21111",
                        "contribution": "10"
                    }]
                }
            },
            {
                "id": "bbb",
                "masterplan": "แผนแม่บทภายใต้ยุทธศาสตร์ชาติที่สอดคล้องกับโครงการ",
                "gold_y1": "เป้าหมายของแผนย่อย",
                "gold_y2": "ป้าหมายของแผนแม่บทประเด็น",
                "gold_z": "เป้าหมายของยุทธศาสตร์ชาติ",
                "z_3digits": "ประเด็นของยุทธศาสตร์ชาติ",
                "z_consistent": "อธิบายความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก",
                "report_gold_y1": {
                    "consistent": "ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย",
                    "indicators": [{
                            "id": "y11111",
                            "contribution": "10"
                        },
                        {
                            "id": "y12222",
                            "contribution": "20"
                        },
                        {
                            "id": "y13333",
                            "contribution": "30"
                        }
                    ]
                },
                "report_gold_y2": {
                    "consistent": "ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย",
                    "indicators": [{
                        "id": "y21111",
                        "contribution": "10"
                    }]
                }
            },
            {
                "id": "ccc",
                "masterplan": "แผนแม่บทภายใต้ยุทธศาสตร์ชาติที่สอดคล้องกับโครงการ",
                "gold_y1": "เป้าหมายของแผนย่อย",
                "gold_y2": "ป้าหมายของแผนแม่บทประเด็น",
                "gold_z": "เป้าหมายของยุทธศาสตร์ชาติ",
                "z_3digits": "ประเด็นของยุทธศาสตร์ชาติ",
                "z_consistent": "อธิบายความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก",
                "report_gold_y1": {
                    "consistent": "ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย",
                    "indicators": [{
                            "id": "y11111",
                            "contribution": "10"
                        },
                        {
                            "id": "y12222",
                            "contribution": "20"
                        },
                        {
                            "id": "y13333",
                            "contribution": "30"
                        }
                    ]
                },
                "report_gold_y2": {
                    "consistent": "ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย",
                    "indicators": [{
                        "id": "y21111",
                        "contribution": "10"
                    }]
                }
            }
        ]
    };

    init_tab1();
}

function init_tab1() {
    tree_masterplan = $('#treeview_masterplan').tree({
        dataSource: 'treeview_jsons/masterplan/masterplan.json',
        primaryKey: 'id',
        checkboxes: false,
        uiLibrary: 'bootstrap',
        textField: "name",
        dataBound: function (e) {
            tree_masterplan.on('select', function (e, node, id) {
                if (id.length <= 4) {
                    return false;
                }
                xyz_template.masterplan = id;
            });
        }
    });
}

function init_tab2() {
    gold_y1 = get_xyz_nextlink(json_directY1_goldY1, xyz_template.masterplan);

    tree_gold_y1 = $('#plan_gold_y1').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name",
    });

    tree_gold_y2 = $('#plan_gold_y2').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name",
    });

    tree_gold_y1.reload();
    build_tree_gold_header(tree_gold_y1, gold_y1);

    tree_gold_y1.on('select', function (e, node, id) {
        xyz_template.gold_y1 = id;

        gold_y2 = get_xyz_nextlink(json_goldY1_goldY2, xyz_template.gold_y1);
        tree_gold_y2.reload();
        build_tree_gold_header(tree_gold_y2, gold_y2);
    });

    tree_gold_y2.on('select', function (e, node, id) {
        xyz_template.gold_y2 = id;
    });

}

function init_tab3() {
    tree_target_z = $('#plan_target_z').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name",
    });

    tree_plan_z = $('#plan_z').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name"
    });

    gold_target_z = get_xyz_nextlink(json_goalY2_goalZ, xyz_template.gold_y2);
    tree_target_z.reload();
    build_tree_z_targer(tree_target_z, gold_target_z);

    tree_target_z.on('select', function (e, node, id) {

        if (id.length <= 4) {
            return false;
        }

        xyz_template.gold_z = id;

        gold_z_z3digit = get_xyz_nextlink(json_goalZ_Z3digits, xyz_template.gold_z);
        direction_y1_z3digit = get_xyz_nextlink(json_directionY1_Z3digits, xyz_template.masterplan);

        z3digit_intersection = intersect(gold_z_z3digit, direction_y1_z3digit);

        tree_plan_z.reload();

        console.log('4-goalZ-Z3digits: ' + gold_z_z3digit.join(', '));
        console.log('5-directionY1-Z3digits: ' + direction_y1_z3digit.join(', '));
        console.log('intersection: ' + z3digit_intersection.join(', '));
        console.log('----------------------------------------------------------------------');

        build_tree_z_intersection(tree_plan_z, z3digit_intersection);

    });

    tree_plan_z.on('select', function (e, node, id) {
        if (id.length < 10) {
            return false;
        }
        console.log(id)

        xyz_template.z_3digits = id;
    });
}

function init_tab4() {
    var _html_z_direct = rander_tree_strategic('strategies', [xyz_template.z_3digits], json_tree_map_strategies, 4);
    var _html_z_target = rander_tree_strategic('strategies', [xyz_template.gold_z], json_tree_map_strategies, 4);

    $('#viewer_z_direct').html(_html_z_direct);
    $('#viewer_z_target').html(_html_z_target);
}

function init_tab5() {
    var gold_y2_interval = get_xyz_nextlink(json_value_goldY1_goldY2, xyz_template.gold_y2);
    var _header_name = get_xyz_text_byID(xyz_template.gold_y2.substr(0, 4));

    $('#viewer_gold_y2_header').text('ความสอดคล้องของโครงการกับเป้าหมายของแผนแม่บทประเด็น ' + _header_name);
    $('#viewer_indicator_y2_header').text('ตัวชี้วัดของแผนแม่บทประเด็น ' + _header_name);

    $('#viewer_gold_y2').html('<strong>เป้าหมาย:</strong>&nbsp;' + get_xyz_text_byID(xyz_template.gold_y2));
    $('#table_gold_y2').empty();

    for (var i = 0; i < gold_y2_interval.length; i++) {
        //console.log(gold_y2_interval[i]);

        var _html = '<label class="control-label control-label-left col-sm-12"><strong>ตัวชี้วัด: </strong>' + get_xyz_text_byID(gold_y2_interval[i].id) + '</label>';

        _html += `
        <div class="col-md-12 row-margin-bottom-20">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-xs-2 text-center">ช่วงปี</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๖๑ - ๒๕๖๕</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๖๖ - ๒๕๗๐</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๗๑ - ๒๕๗๕</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๗๖ - ๒๕๘๐</th>
                    </tr>
                </thead>
                <tr>
                    <td class="text-center">ค่าเป้าหมาย</td>
                    `;

        _html += '<td class="text-center" id="viewer_gold_y2_interval_1">' + gold_y2_interval[i].target[0] + '</td>';
        _html += '<td class="text-center" id="viewer_gold_y2_interval_2">' + gold_y2_interval[i].target[1] + '</td>';
        _html += '<td class="text-center" id="viewer_gold_y2_interval_3">' + gold_y2_interval[i].target[2] + '</td>';
        _html += '<td class="text-center" id="viewer_gold_y2_interval_4">' + gold_y2_interval[i].target[3] + '</td>';

        _html += `</tr>
            </table>
            <div class="input-group">
                <span class="input-group-addon">กรุณาระบุสัดส่วน Contribution ต่อเป้าหมายเมื่อเสร็จสิ้นโครงการ</span>
                `;

        _html += '<input type="text" id="textbox_gold_y2_contribution_' + gold_y2_interval[i].id + '" class="form-control" value="0.00">';
        _html += `<div class="input-group-addon">%</div>
            </div>
        </div>
      `;

        $('#table_gold_y2').append(_html);
    }

}

function init_tab6() {
    var gold_y1_interval = get_xyz_nextlink(json_value_goldY1_goldY2, xyz_template.gold_y1);

    var _header_name = get_xyz_text_byID(xyz_template.gold_y1.substr(0, 4));

    $('#viewer_gold_y1_header').text('ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย ' + _header_name);
    $('#viewer_indicator_y1_header').text('ตัวชี้วัดของแผนย่อย ' + _header_name);

    $('#viewer_gold_y1').html('<strong>เป้าหมาย:</strong>&nbsp;' + get_xyz_text_byID(xyz_template.gold_y1));
    $('#table_gold_y1').empty();

    for (var i = 0; i < gold_y1_interval.length; i++) {
        var _html = '<label class="control-label control-label-left col-sm-12"><strong>ตัวชี้วัด: </strong>' + get_xyz_text_byID(gold_y1_interval[i].id) + '</label>';

        _html += `
        <div class="col-md-12 row-margin-bottom-20">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-xs-2 text-center">ช่วงปี</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๖๑ - ๒๕๖๕</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๖๖ - ๒๕๗๐</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๗๑ - ๒๕๗๕</th>
                        <th class="col-xs-2 text-center">ปี ๒๕๗๖ - ๒๕๘๐</th>
                    </tr>
                </thead>
                <tr>
                    <td class="text-center">ค่าเป้าหมาย</td>
                    `;

        _html += '<td class="text-center" id="viewer_gold_y1_interval_1">' + gold_y1_interval[i].target[0] + '</td>';
        _html += '<td class="text-center" id="viewer_gold_y1_interval_2">' + gold_y1_interval[i].target[1] + '</td>';
        _html += '<td class="text-center" id="viewer_gold_y1_interval_3">' + gold_y1_interval[i].target[2] + '</td>';
        _html += '<td class="text-center" id="viewer_gold_y1_interval_4">' + gold_y1_interval[i].target[3] + '</td>';

        _html += `</tr>
            </table>
            <div class="input-group">
                <span class="input-group-addon">กรุณาระบุสัดส่วน Contribution ต่อเป้าหมายเมื่อเสร็จสิ้นโครงการ</span>
                `;

        _html += '<input type="text" id="textbox_gold_y1_contribution_' + gold_y1_interval[i].id + '" class="form-control" value="0.00">';
        _html += `<div class="input-group-addon">%</div>
            </div>
        </div>
      `;

        $('#table_gold_y1').append(_html);
    }
}

function init_tab7() {
    console.log(xyz_template);

    var _html_z_direct = rander_tree_strategic('strategies', [xyz_template.z_3digits], json_tree_map_strategies, 4);
    var _html_z_target = rander_tree_strategic('strategies', [xyz_template.gold_z], json_tree_map_strategies, 4);

    $('#viewer_preview_z_direct').html(_html_z_direct);
    $('#viewer_preview_z_target').html(_html_z_target);
    $('#viewer_preview_z_consistent').text(xyz_template.report_z_consistent);

    $('#viewer_preview_y2_header').text(get_xyz_text_byID(xyz_template.gold_y2.substr(0, 4)));
    $('#viewer_preview_gold_y2').text(get_xyz_text_byID(xyz_template.gold_y2));
    $('#viewer_preview_gold_y2_consistent').text(xyz_template.report_gold_y2.consistent);
    $('#viewer_preview_gold_y2_indicators').html(create_table_y_indicator(xyz_template.report_gold_y2.indicators));

    $('#viewer_preview_y1_header').text(get_xyz_text_byID(xyz_template.gold_y1.substr(0, 4)));
    $('#viewer_preview_masterplan').text(get_xyz_text_byID(xyz_template.masterplan));
    $('#viewer_preview_gold_y1').text(get_xyz_text_byID(xyz_template.gold_y1));
    $('#viewer_preview_gold_y1_consistent').text(xyz_template.report_gold_y1.consistent);
    $('#viewer_preview_gold_y1_indicators').html(create_table_y_indicator(xyz_template.report_gold_y1.indicators));

}

function get_z_title(_node_id) {
    var _z_title = '';

    if (_node_id == 'Y10') {
        _z_title = 'ยุทธศาสตร์ชาติด้านความมั่นคง';
    } else if (_node_id == 'Y20') {
        _z_title = 'ยุทธศาสตร์ชาติด้านการสร้างความสามารถในการแข่งขัน';
    } else if (_node_id == 'Y30') {
        _z_title = 'ยุทธศาสตร์ด้านการพัฒนาและเสริมสร้างศักยภาพมนุษย์';
    } else if (_node_id == 'Y40') {
        _z_title = 'ยุทธศาสตร์ชาติด้านการสร้างโอกาสและความเสมอภาคทางสังคม';
    } else if (_node_id == 'Y50') {
        _z_title = 'ยุทธศาสตร์ชาติด้านการสร้างการเติบโตบนคุณภาพชีวิตที่เป็นมิตรต่อสิ่งแวดล้อม';
    } else if (_node_id == 'Y60') {
        _z_title = 'ยุทธศาสตร์ชาติด้านการปรับสมดุลและพัฒนาระบบการบริหารจัดการภาครัฐ';
    } else {
        _z_title = 'ยุทธศาสตร์ชาติด้านความมั่นคง';
    }

    return _z_title;
}

/***************** Load json data *************************/

function load_directY1_goldY1() {
    var _read_data = JSON.parse($.getJSON({
        'url': "treeview_jsons/v2/xyz/1-directionY1-goalY1.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_goalY1_goalY2() {
    var _read_data = JSON.parse($.getJSON({
        'url': "treeview_jsons/v2/xyz/2-goalY1-goalY2.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_goalY2_goalZ() {
    var _read_data = JSON.parse($.getJSON({
        'url': "treeview_jsons/v2/xyz/3-goalY2-goalZ.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_goalZ_Z3digits() {
    var _read_data = JSON.parse($.getJSON({
        'url': "treeview_jsons/v2/xyz/4-goalZ-Z3digits.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_directionY1_Z3digits() {
    var _read_data = JSON.parse($.getJSON({
        'url': "treeview_jsons/v2/xyz/5-directionY1-Z3digits.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_value_goldY1_goldY2() {
    var _read_data = JSON.parse($.getJSON({
        'url': "treeview_jsons/v2/xyz/value-goldY1-goldY2.json",
        'async': false
    }).responseText);

    return _read_data;
}

/******************************************/
function build_tree_gold(_tree, _left_node) {

    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {
        _tree.addNode({
            id: _left_node[i],
            name: get_xyz_text_byID(_left_node[i]),
            "imageCssClass": "glyphicon glyphicon-chevron-right"
        });
    }
}

function build_tree_gold_header(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {
        var _y_4d_id = _left_node[i].substr(0, 4);

        if (typeof _tree.getNodeById(_y_4d_id) == 'undefined') {
            _tree.addNode({
                id: _y_4d_id,
                name: get_xyz_text_byID(_y_4d_id)
            });
        }
        _parent = _tree.getNodeById(_y_4d_id);
        _tree.addNode({
            id: _left_node[i],
            name: get_xyz_text_byID(_left_node[i]),
            "imageCssClass": "glyphicon glyphicon-chevron-right"
        }, _parent);
    }
}

function build_tree_z_targer(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {

        if (typeof _tree.getNodeById(_left_node[i]) == 'undefined') {
            var _z_3d_id = _left_node[i].substr(0, 4);
            var _id = _left_node[i];
            var _name = tree_get_name_strategies(json_tree_map_strategies, _left_node[i]);

            if (typeof _tree.getNodeById(_z_3d_id) == 'undefined') {

                var _parent = _tree.getNodeById(_z_3d_id);
                _tree.addNode({
                    id: _z_3d_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_3d_id)
                }, _parent);

            }

            var _parent = _tree.getNodeById(_z_3d_id);
            _tree.addNode({
                id: _id,
                name: _name,
                "imageCssClass": "glyphicon glyphicon-chevron-right"
            }, _parent);
        }
    }
}

function build_tree_z_intersection(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {

        if (typeof _tree.getNodeById(_left_node[i]) == 'undefined') {

            var _z_3d_id = _left_node[i].substr(0, 4);
            var _z_mid_id = _left_node[i].substr(0, 8);

            var _id = _left_node[i];
            var _name = tree_get_name_strategies(json_tree_map_strategies, _left_node[i]);

            if (typeof _tree.getNodeById(_z_3d_id) == 'undefined') {
                _tree.addNode({
                    id: _z_3d_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_3d_id)
                }, _root);

            }

            if (typeof _tree.getNodeById(_z_mid_id) == 'undefined') {
                var _root = _tree.getNodeById(_z_3d_id);

                _tree.addNode({
                    id: _z_mid_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_mid_id)
                }, _root);
            }

            var _parent = _tree.getNodeById(_z_mid_id);

            _tree.addNode({
                id: _id,
                name: _name,
                "imageCssClass": "glyphicon glyphicon-chevron-right"
            }, _parent);
        }
    }
}

function get_xyz_nextlink(_obj, _current_id) {
    if (_obj[_current_id] === undefined) {
        return -1;
    } else {
        return _obj[_current_id];
    }
}

function get_xyz_text_byID(_id) {
    if (xyz_kv[_id] === undefined) {
        return -1;
    } else {
        return xyz_kv[_id];
    }
}

function create_table_y_indicator(_indicators) {
    if (_indicators.length > 0) {
        var _temp_text = '<table class="table table-condensed"><thead><tr><th>ตัวชี้วัด</th><th>Contribution ต่อเป้าหมายเมื่อเสร็จสิ้นโครงการ (%)</th></tr></thead><tbody>';

        for (var i = 0; i < _indicators.length; i++) {

            _temp_text += '<tr><td>';
            _temp_text += get_xyz_text_byID(_indicators[i].id);
            _temp_text += '</td><td>';
            _temp_text += _indicators[i].contribution;
            _temp_text += '</td></tr>';
        }
        _temp_text += '</tbody></table>';

        return _temp_text;

    } else {
        return 'ไม่มีข้อมูลตัวชี้วัด';
    }
}

function confirm_remove_xyz(_type, _id) {
    if (_type == 'direct') {
        xyz_state_remove = 'direct';

        $('#xyz_confirm_delete_header').text('ยืนยันการลบข้อมูลแนวทางการพัฒนาหลัก');
        $('#xyz_confirm_delete_message').text(get_xyz_text_byID(xyz_direct.masterplan));
    } else {
        xyz_state_remove = 'secondary';

        $('#xyz_confirm_delete_header').text('ยืนยันการลบข้อมูลแนวทางการพัฒนารอง');
    }

    $('#xyz_confirm_delete').modal('show');
}

function intersect(a, b) {
    if (a == -1 || b == -1) {
        return [];
    }

    var setA = new Set(a);
    var setB = new Set(b);
    var intersection = new Set([...setA].filter(x => setB.has(x)));
    return Array.from(intersection);
}

function get_xyz_mid(_xyz) {
    return _xyz.masterplan + _xyz.gold_y1 + _xyz.gold_y2 + _xyz.gold_z + _xyz.z_3digits;

}