/* v1.2019.10.30.1 */

/* Treeview collapse 1 */
$("#collapseOne").on("show.bs.collapse", function () {
    // Open
    $('#nav_pills_tab_header_strategies').addClass("nav_pills_tab_strategies_active");
});

$("#collapseOne").on("hide.bs.collapse", function () {
    // Close
    $('#nav_pills_tab_header_strategies').removeClass("nav_pills_tab_strategies_active");
});

/* Treeview collapse 3 */
$("#collapseThree").on("show.bs.collapse", function () {
    // Open
    $('#nav_pills_tab_header_government_ministers').addClass("nav_pills_tab_government_active");
});

$("#collapseThree").on("hide.bs.collapse", function () {
    // Close
    $('#nav_pills_tab_header_government_ministers').removeClass("nav_pills_tab_government_active");
});

/* Treeview collapse 2 */

$("#collapseTwo").on("show.bs.collapse", function (e) {
    // Open
    //var nav_pills_tab_id_active = $("ul#nav_pills_tab_header_planreform li.active").attr('id');
});

$("#collapseTwo").on("hide.bs.collapse", function () {
    // Close
});

$('[id^="nav_pills_tab_planreform_tab_header_"]').click(function () {
    if ($('#collapseTwo').hasClass('collapse in')) {
        // Open
        if ($(this).hasClass('active')) {
            $('#collapseTwo').collapse('hide');
        }

    } else {
        $('#collapseTwo').collapse('show');
    }

});

var tree_strategies_option = null;
var tree_planreform_option = null;
var tree_plan12 = null;
var tree_homeland_policy = null;

function inti_treeview_all(_update_page) {

    //$(".preload").fadeOut(100);
    //$("#main_content, #footer_content").fadeIn(1000);

    tree_strategies_option = $('#treeview_strategies_option').tree({
        //dataSource: 'assets/treeview_jsons/v2/strategies/1.homeland_security.json',
        dataSource: 'assets/treeview_jsons/v2/strategies/strategies_mixed.json',
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {}
    });

    tree_planreform_option = $('#treeview_planreform_option').tree({
        //dataSource: 'assets/treeview_jsons/v2/planreform11/anticorruption.json',
        dataSource: 'assets/treeview_jsons/v2/planreform11/planreform11_mixed.json',
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {
            $(".preload").fadeOut(100);
            $("#main_content, #footer_content").fadeIn(1000);

            load_form_data()
        }
    });

    tree_plan12 = $('#treeview_plan12').tree({
        dataSource: 'assets/treeview_jsons/v2/plan12/plan12.json',
        primaryKey: 'id',
        checkboxes: true,
        uiLibrary: 'bootstrap',
        textField: "name",
        dataBound: function (e) {}
    });

    tree_homeland_policy = $('#treeview_homeland_policy_option').tree({
        dataSource: 'assets/treeview_jsons/v2/homeland/homeland_policy.json',
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {}
    });

    tree_government_policy = $('#treeview_government_policy_main').tree({
        dataSource: 'assets/treeview_jsons/v2/government_policy/gov_policy_main.json',
        primaryKey: 'id',
        checkboxes: true,
        uiLibrary: 'bootstrap',
        textField: "name",
        dataBound: function (e) {}
    });

}

function expand_first_level(_tree, _root_text) {
    var node = _tree.getNodeByText(_root_text);
    _tree.expand(node);
}

var count_check_planreform_option = 0;
var seq_node_tree_planreform_option = [];

var _old_count_event_strategies = 0;


// Expand/collapse all

$('#treeview_planreform_main_btn_collapse_all').on('click', function (e) {
    tree_planreform_main.collapseAll();
});

$('#treeview_planreform_option_btn_collapse_all').on('click', function (e) {
    tree_planreform_option.collapseAll();
});

$('#treeview_plan12_btn-expand-all').on('click', function (e) {
    tree_plan12.expandAll();
});

$('#treeview_plan12_btn-collapse-all').on('click', function (e) {
    tree_plan12.collapseAll();
});


var tree_strategies_main_dynamic = null;
var tree_planreform_main_dynamic = null;

var old_strategies_root_id_selected = '';
var old_reform_root_id_selected = '';


$("#div_dropdown_treeview_strategies_planreform .dropdown-menu li a").click(function () {
    $("#space_planreform_main_strategies_dynamic").hide();
    $("#space_planreform_main_strategies_loading").show();

    var dropdown_treeview_strategies_value = $(this).parents().data('value');
    var dropdown_treeview_strategies_text = $(this).text();

    //tree_strategies_main_dynamic = null;

    if (tree_strategies_main_dynamic != null) {
        tree_strategies_main_dynamic.destroy();
        tree_strategies_main_dynamic = null;
    }

    var json_file_name = 'assets/treeview_jsons/v2/strategies/' + dropdown_treeview_strategies_value;


    tree_strategies_main_dynamic = $('#treeview_planreform_main_strategies_dynamic').tree({
        dataSource: json_file_name,
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {
            $("#space_planreform_main_strategies_loading").hide();
            $("#space_planreform_main_strategies_dynamic").show();
            var strategies_main_header_text = 'รายละเอียด' + dropdown_treeview_strategies_text;
            $("#header_planreform_main_strategies_dynamic").text(strategies_main_header_text);

            if (get_strategies_root_id(dropdown_treeview_strategies_value).length > 0) {
                if (old_strategies_root_id_selected.length > 0) {
                    tree_strategies_option.enable(tree_strategies_option.getNodeById(get_strategies_root_id(old_strategies_root_id_selected)));
                }

                tree_strategies_option.disable(tree_strategies_option.getNodeById(get_strategies_root_id(dropdown_treeview_strategies_value)));
            }
            old_strategies_root_id_selected = dropdown_treeview_strategies_value;

        }
    });

});

$("#div_dropdown_treeview_planreform .dropdown-menu li a").click(function () {
    $("#space_planreform_main_dynamic").hide();
    $("#space_planreform_main_loading").show();

    var dropdown_treeview_planreform_value = $(this).parents().data('value');
    var dropdown_treeview_planreform_text = $(this).text();

    if (tree_planreform_main_dynamic != null) {
        tree_planreform_main_dynamic.destroy();
        tree_planreform_main_dynamic = null;
    }

    var json_file_name = 'assets/treeview_jsons/v2/planreform11/' + dropdown_treeview_planreform_value;

    tree_planreform_main_dynamic = $('#treeview_planreform_main_dynamic').tree({
        dataSource: json_file_name,
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {
            $("#space_planreform_main_loading").hide();
            $("#space_planreform_main_dynamic").show();
            var planreform_main_header_text = 'รายละเอียด' + dropdown_treeview_planreform_text;
            $("#header_planreform_main_dynamic").text(planreform_main_header_text);
            $('#block_reform_option').show();

            if (get_reform_root_id(dropdown_treeview_planreform_value).length > 0) {
                if (old_reform_root_id_selected.length > 0) {
                    tree_planreform_option.enable(tree_planreform_option.getNodeById(get_reform_root_id(old_reform_root_id_selected)));
                }

                tree_planreform_option.disable(tree_planreform_option.getNodeById(get_reform_root_id(dropdown_treeview_planreform_value)));
            }
            old_reform_root_id_selected = dropdown_treeview_planreform_value;
        }
    });
});

/********** Auto check tree from XYZ data **********/

function auto_check_tree_strategies_direct(_tree_selected) {
    $("#space_planreform_main_strategies_dynamic").hide();
    $("#space_planreform_main_strategies_loading").show();

    var dropdown_treeview_strategies_value = get_filename_strategies(_tree_selected[0]);
    var dropdown_treeview_strategies_text = get_category_strategies(_tree_selected[0]);

    if (tree_strategies_main_dynamic != null) {
        tree_strategies_main_dynamic.destroy();
        tree_strategies_main_dynamic = null;
    }

    $("#btn_treeview_trategies_planreform").text(dropdown_treeview_strategies_text);

    var json_file_name = 'assets/treeview_jsons/v2/strategies/' + dropdown_treeview_strategies_value;

    tree_strategies_main_dynamic = $('#treeview_planreform_main_strategies_dynamic').tree({
        dataSource: json_file_name,
        checkboxes: true,
        primaryKey: 'id',
        textField: "name",
        uiLibrary: 'bootstrap',
        dataBound: function (e) {
            $("#space_planreform_main_strategies_loading").hide();
            $("#space_planreform_main_strategies_dynamic").show();

            var strategies_main_header_text = 'รายละเอียด' + dropdown_treeview_strategies_text;
            $("#header_planreform_main_strategies_dynamic").text(strategies_main_header_text);

            for (var i = 0; i < _tree_selected.length; i++) {
                tree_strategies_expand_selected('main', _tree_selected[i]);
            }

            tree_strategies_main_dynamic.disableAll();
        }
    });

}

function auto_check_tree_strategies_secondary(_tree_selected) {
    tree_strategies_option.uncheckAll();
    tree_strategies_option.collapseAll();

    for (var i = 0; i < _tree_selected.length; i++) {
        tree_strategies_expand_selected('option', _tree_selected[i]);
    }

    tree_strategies_option.disableAll();
}


function get_filename_strategies(_node_id) {
    var sub_node_id = _node_id.substring(0, 3);

    if (sub_node_id == 'Y10') {
        return '1.homeland_security.json';
    } else if (sub_node_id == 'Y20') {
        return '2.competitiveness.json';
    } else if (sub_node_id == 'Y30') {
        return '3.human_development.json';
    } else if (sub_node_id == 'Y40') {
        return '4.social_equality.json';
    } else if (sub_node_id == 'Y50') {
        return '5.ecofriendly.json';
    } else if (sub_node_id == 'Y60') {
        return '6.govadmin_development.json';
    } else {
        return '';
    }
}

function get_category_strategies(_node_id) {
    var sub_node_id = _node_id.substring(0, 3);

    if (sub_node_id == 'Y10') {
        return 'ยุทธศาสตร์ชาติด้านความมั่นคง';
    } else if (sub_node_id == 'Y20') {
        return 'ยุทธศาสตร์ชาติด้านการสร้างความสามารถในการแข่งขัน';
    } else if (sub_node_id == 'Y30') {
        return 'ยุทธศาสตร์ชาติด้านการพัฒนาและเสริมสร้างศักยภาพทรัพยากรมนุษย์';
    } else if (sub_node_id == 'Y40') {
        return 'ยุทธศาสตร์ชาติด้านการสร้างโอกาสและความเสมอภาคทางสังคม';
    } else if (sub_node_id == 'Y50') {
        return 'ยุทธศาสตร์ชาติด้านการสร้างการเติบโตบนคุณภาพชีวิตที่เป็นมิตรต่อสิ่งแวดล้อม';
    } else if (sub_node_id == 'Y60') {
        return 'ยุทธศาสตร์ชาติด้านการปรับสมดุลและพัฒนาระบบการบริหารจัดการภาครัฐ';
    } else {
        return '';
    }
}

function tree_strategies_expand_selected(_type, _check_index) {
    var _id_arr = [];
    var _temp_id = '';

    if (_check_index.length < 4) {
        return;
    }

    _id_arr.push(_check_index.substring(0, 4));

    for (i = 6; i <= _check_index.length; i += 2) {
        _id_arr.push(_check_index.substring(i - 2, i));
    }

    for (i = 0; i < _id_arr.length; i++) {
        _temp_id = _temp_id + _id_arr[i];

        if (_type == 'main') {
            tree_strategies_main_dynamic.expand(tree_strategies_main_dynamic.getNodeById(_temp_id), false);
        } else if (_type == 'option') {
            tree_strategies_option.expand(tree_strategies_option.getNodeById(_temp_id), false);
        }
    }

    if (_type == 'main') {
        tree_strategies_main_dynamic.check(tree_strategies_main_dynamic.getNodeById(_check_index));
    } else if (_type == 'option') {
        tree_strategies_option.check(tree_strategies_option.getNodeById(_check_index));
    }

}

/********** Helper function for tree **********/

function enanble_strategies_root_id_all() {
    var _strategies_root_id = ['Y101', 'Y201', 'Y301', 'Y401', 'Y501', 'Y601'];

    for (var i = 0; i < _strategies_root_id.length; i++) {
        tree_strategies_option.enable(tree_strategies_option.getNodeById(_strategies_root_id[i]));
    }
}

function enanble_reform_root_id_all() {
    var _reform_root_id = ['A01', 'B01', 'C01', 'D01', 'E01', 'F01', 'G01', 'H01', 'I01', 'J01', 'K01'];

    for (var i = 0; i < _reform_root_id.length; i++) {
        tree_planreform_option.enable(tree_planreform_option.getNodeById(_reform_root_id[i]));
    }
}

function get_strategies_root_id(_text) {
    if (_text == '1.homeland_security.json') {
        return 'Y101';
    } else if (_text == '2.competitiveness.json') {
        return 'Y201';
    } else if (_text == '3.human_development.json') {
        return 'Y301';
    } else if (_text == '4.social_equality.json') {
        return 'Y401';
    } else if (_text == '5.ecofriendly.json') {
        return 'Y501';
    } else if (_text == '6.govadmin_development.json') {
        return 'Y601';
    } else {
        return '';
    }
}

function get_reform_root_id(_text) {
    if (_text == 'anticorruption.json') {
        return 'A01';
    } else if (_text == 'economic.json') {
        return 'B01';
    } else if (_text == 'ecosystem.json') {
        return 'C01';
    } else if (_text == 'energy.json') {
        return 'D01';
    } else if (_text == 'govadmin.json') {
        return 'E01';
    } else if (_text == 'healthcare.json') {
        return 'F01';
    } else if (_text == 'justice.json') {
        return 'G01';
    } else if (_text == 'law.json') {
        return 'H01';
    } else if (_text == 'media.json') {
        return 'I01';
    } else if (_text == 'politics.json') {
        return 'J01';
    } else if (_text == 'social.json') {
        return 'K01';
    } else if (_text == 'education.json') {
        return 'M01';
    } else {
        return '';
    }
}


function check_tree_is_valid() {
    var _valid = true;
    var validate_tree_not_leaf = null;

    if (tree_strategies_main_dynamic != null) {
        validate_tree_not_leaf = tree_leaf_node_check(tree_strategies_main_dynamic, tree_strategies_main_dynamic.getCheckedNodes());
        if (!validate_tree_show_alert(validate_tree_not_leaf, '#alert_tab1_treeview_strategies_direct')) {
            _valid = false;
        }
    }

    validate_tree_not_leaf = tree_leaf_node_check(tree_strategies_option, tree_strategies_option.getCheckedNodes());

    if (!validate_tree_show_alert(validate_tree_not_leaf, '#alert_tab1_treeview_strategies_option')) {
        _valid = false;
    }

    if (tree_planreform_main_dynamic != null) {
        validate_tree_not_leaf = tree_leaf_node_check(tree_planreform_main_dynamic, tree_planreform_main_dynamic.getCheckedNodes());
        if (!validate_tree_show_alert(validate_tree_not_leaf, '#alert_tab1_treeview_reform_direct')) {
            _valid = false;
        }
    }

    validate_tree_not_leaf = tree_leaf_node_check(tree_planreform_option, tree_planreform_option.getCheckedNodes());

    if (!validate_tree_show_alert(validate_tree_not_leaf, '#alert_tab1_treeview_reform_option')) {
        _valid = false;
    }

    validate_tree_not_leaf = tree_leaf_node_check(tree_plan12, tree_plan12.getCheckedNodes());

    if (!validate_tree_show_alert(validate_tree_not_leaf, '#alert_tab1_treeview_plan12')) {
        _valid = false;
    }

    validate_tree_not_leaf = tree_leaf_node_check(tree_homeland_policy, tree_homeland_policy.getCheckedNodes());

    if (!validate_tree_show_alert(validate_tree_not_leaf, '#alert_tab1_treeview_homeland_policy')) {
        _valid = false;
    }

    tree_leaf_node_gov_policy_check();

    return _valid;
}

function tree_leaf_node_gov_policy_check() {
    var _tree_checked_node = tree_government_policy.getCheckedNodes();
    var _tree_checked_node_sort = _tree_checked_node.sort();
    var _temp_completed = [];

    if (_tree_checked_node_sort.length == 0) {
        return false;
    }

    for (var i = 0; i < _tree_checked_node_sort.length; i++) {
        for (var j = 0; j < _tree_checked_node_sort.length; j++) {
            if (i == j) {
                continue;
            }

            if (tree_government_policy.getDataById(_tree_checked_node_sort[i])._checkenabled == true) {
                _temp_completed.push(_tree_checked_node_sort[i]);
                break;
            } else if (_tree_checked_node_sort[j].search(_tree_checked_node_sort[i]) > -1) {
                _temp_completed.push(_tree_checked_node_sort[i]);
                break;
            }
        }
    }

    if (_tree_checked_node_sort.length == _temp_completed.length) {
        return true;
    } else {
        return false;
    }
}


function tree_leaf_node_check(_tree, _tree_checked_node) {

    var _tree_checked_node_sort = _tree_checked_node.sort();
    var node_last_check = false;

    var node_id_unique_long = [];
    var node_name_not_leaf = [];

    for (i = 0; i < _tree_checked_node_sort.length; i++) {

        var _temp_leaf = false;

        for (j = i; j < _tree_checked_node_sort.length; j++) {
            if (i == j) {
                continue;
            }

            if (_tree_checked_node_sort[j].indexOf(_tree_checked_node_sort[i]) >= 0) {
                _temp_leaf = false;
                break;
            } else {
                _temp_leaf = true;
            }
        }

        if (_temp_leaf) {
            node_id_unique_long.push(_tree_checked_node_sort[i]);
        }

        if (i == _tree_checked_node_sort.length - 1) {
            node_id_unique_long.push(_tree_checked_node_sort[i]);
        }
    }

    for (k = 0; k < node_id_unique_long.length; k++) {
        var _temp_node_data = _tree.getDataById(node_id_unique_long[k]);

        if ((_temp_node_data._checkenabled == true)) {
            continue;
        } else {
            if (_temp_node_data.hasOwnProperty('children')) {
                if (_temp_node_data.children.length == 0) {
                    continue;
                }
            }

            node_name_not_leaf.push(_temp_node_data.name);
        }
    }

    return node_name_not_leaf;
}

function validate_tree_show_alert(_validate_tree_not_leaf, _alert_id) {
    var _valid = true;
    var _temp_html = '';

    if (_alert_id == '#alert_tab1_treeview_strategies_direct') {
        if (_validate_tree_not_leaf.length > 0) {
            _valid = false;
            _temp_html = '<strong>ผิดพลาด!</strong> จำเป็นต้องเลือก<strong>แผนระดับที่ 1 ยุทธศาสตร์ชาติที่เกี่ยวข้องโดยตรง</strong> ให้ลึกที่สุด ยุทธศาสตร์ชาติที่จำเป็นต้องเลือกต่อประกอบไปด้วย';
            _temp_html += '<ul>';

            for (i = 0; i < _validate_tree_not_leaf.length; i++) {
                _temp_html += '<li>' + _validate_tree_not_leaf[i] + '</li>';
            }

            _temp_html += '</ul>';

            $(_alert_id).html(_temp_html);
            $(_alert_id).show();
        } else {
            $(_alert_id).hide();
        }

    } else if (_alert_id == '#alert_tab1_treeview_strategies_option') {
        if (_validate_tree_not_leaf.length > 0) {
            _valid = false;
            _temp_html = '<strong>ผิดพลาด!</strong> จำเป็นต้องเลือก<strong>แผนระดับที่ 1 ยุทธศาสตร์ชาติที่เกี่ยวข้องในระดับรอง</strong> ให้ลึกที่สุด ยุทธศาสตร์ชาติที่จำเป็นต้องเลือกต่อประกอบไปด้วย';
            _temp_html += '<ul>';

            for (i = 0; i < _validate_tree_not_leaf.length; i++) {
                _temp_html += '<li>' + _validate_tree_not_leaf[i] + '</li>';
            }

            _temp_html += '</ul>';

            $(_alert_id).html(_temp_html);
            $(_alert_id).show();
        } else {
            $(_alert_id).hide();
        }

    } else if (_alert_id == '#alert_tab1_treeview_reform_direct') {
        if (_validate_tree_not_leaf.length > 0) {
            _valid = false;
            _temp_html = '<strong>ผิดพลาด!</strong> จำเป็นต้องเลือก<strong>แผนระดับที่ 2 แผนปฏิรูปประเทศด้านที่เกี่ยวข้องโดยตรง</strong> ให้ลึกที่สุด แผนปฏิรูปประเทศที่จำเป็นต้องเลือกต่อประกอบไปด้วย';
            _temp_html += '<ul>';

            for (i = 0; i < _validate_tree_not_leaf.length; i++) {
                _temp_html += '<li>' + _validate_tree_not_leaf[i] + '</li>';
            }

            _temp_html += '</ul>';

            $(_alert_id).html(_temp_html);
            $(_alert_id).show();
        } else {
            $(_alert_id).hide();
        }

    } else if (_alert_id == '#alert_tab1_treeview_reform_option') {
        if (_validate_tree_not_leaf.length > 0) {
            _valid = false;
            _temp_html = '<strong>ผิดพลาด!</strong> จำเป็นต้องเลือก<strong>แผนระดับที่ 2 แผนปฏิรูปประเทศด้านที่เกี่ยวข้องในระดับรอง</strong> ให้ลึกที่สุด แผนปฏิรูปประเทศที่จำเป็นต้องเลือกต่อประกอบไปด้วย';
            _temp_html += '<ul>';

            for (i = 0; i < _validate_tree_not_leaf.length; i++) {
                _temp_html += '<li>' + _validate_tree_not_leaf[i] + '</li>';
            }

            _temp_html += '</ul>';

            $(_alert_id).html(_temp_html);
            $(_alert_id).show();
        } else {
            $(_alert_id).hide();
        }
    } else if (_alert_id == '#alert_tab1_treeview_plan12') {
        if (_validate_tree_not_leaf.length > 0) {
            _valid = false;
            _temp_html = '<strong>ผิดพลาด!</strong> จำเป็นต้องเลือก<strong>แผนระดับที่ 2 แผนพัฒนาเศรษฐกิจและสังคมแห่งชาติ ฉบับที่ 12</strong> ให้ลึกที่สุด แผนพัฒนาเศรษฐกิจและสังคมแห่งชาติ ฉบับที่ 12 ที่จำเป็นต้องเลือกต่อประกอบไปด้วย';
            _temp_html += '<ul>';

            for (i = 0; i < _validate_tree_not_leaf.length; i++) {
                _temp_html += '<li>' + _validate_tree_not_leaf[i] + '</li>';
            }

            _temp_html += '</ul>';

            $(_alert_id).html(_temp_html);
            $(_alert_id).show();
        } else {
            $(_alert_id).hide();
        }
    } else if (_alert_id == '#alert_tab1_treeview_homeland_policy') {
        if (_validate_tree_not_leaf.length > 0) {
            _valid = false;
            _temp_html = '<strong>ผิดพลาด!</strong> จำเป็นต้องเลือก<strong>แผนระดับที่ 2 แผนความมั่นคงแห่งชาติ</strong> ให้ลึกที่สุด แผนความมั่นคงแห่งชาติที่จำเป็นต้องเลือกต่อประกอบไปด้วย';
            _temp_html += '<ul>';

            for (i = 0; i < _validate_tree_not_leaf.length; i++) {
                _temp_html += '<li>' + _validate_tree_not_leaf[i] + '</li>';
            }

            _temp_html += '</ul>';

            $(_alert_id).html(_temp_html);
            $(_alert_id).show();
        } else {
            $(_alert_id).hide();
        }
    }

    return _valid;

}