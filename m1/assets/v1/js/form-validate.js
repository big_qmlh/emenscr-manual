/* v1.2019.10.30.1 */

function validate_tab1_all() {
    var _valid = true;

    get_data_tab_1();
    init_alert_on_page_start_tab_1();

    if (typeof state_result_tab1[0].masterplan_direct.masterplan !== "undefined") {
        $('#div_info_strategies').hide();
        $('#div_block_strategies_direct').show();
        $('#div_block_strategies_option').show();

        $('#nav_pills_tab_header_strategies').removeClass('nav_pills_tab_disable');
        $('#nav_pills_tab_header_strategies').attr('data-toggle', 'tab');
        $('#nav_pills_tab_header_strategies').attr('title', '');

        $('#block_xyz_option').show();

        if (typeof state_result_tab1[0].masterplan_secondary[0] !== "undefined") {
            $('#div_block_strategies_option').show();
            $('#block_xyz_option').show();
        } else {
            $('#div_block_strategies_option').hide();
            $('#block_xyz_option').show();
        }
    } else {
        $('#div_info_strategies').show();
        $('#div_block_strategies_direct').hide();
        $('#div_block_strategies_option').hide();

        $('#block_xyz_option').hide();

        $('#nav_pills_tab_header_strategies').addClass('nav_pills_tab_disable');
        $('#nav_pills_tab_header_strategies').attr('data-toggle', 'tooltip');
        $('#nav_pills_tab_header_strategies').attr('title', 'ยุทธศาสตร์ชาติจะถูกเชื่อมโยงผ่านแผนแม่บทภายใต้ยุทธศาสตร์ชาติให้โดยอัตโนมัติ กรุณาเลือกแผนแม่บทภายใต้ยุทธศาสตร์ชาติด้านล่าง');

        // Show error message.
        $("#alert_tab1_treeview_no_level_2").html('<strong>ผิดพลาด!</strong> จำเป็นต้องเลือกแนวทางการพัฒนาของแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่เกี่ยวข้องโดยตรง อย่างน้อย 1 แนวทาง');
        $("#alert_tab1_treeview_no_level_2").show();

        _valid = false;

    }

    if (!check_tree_is_valid()) {
        _valid = false;
    }

    return _valid;
}
/***************** Validate XYZ form *************************/

function validate_xyz_step_1() {
    var _valid = true;

    reset_validate_xyz_step_1();

    if ($('#select_masterplan_title').val() == -1 || $('#select_masterplan_title').val() == null) {
        _valid = false;
        $("#xyz_error_select_masterplan_title").text('กรุณาเลือกแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่เกี่ยวข้อง');
    }

    if ($('#select_masterplan_subplan').val() == -1 || $('#select_masterplan_subplan').val() == null) {
        _valid = false;
        $("#xyz_error_select_masterplan_subplan").text('กรุณาเลือกแผนย่อยที่เกี่ยวข้อง');
    }

    if ($('#select_masterplan_goal').val() == -1 || $('#select_masterplan_goal').val() == null) {
        _valid = false;
        $("#xyz_error_select_masterplan_goal").text('กรุณาเลือกเป้าหมายแผนย่อยที่เกี่ยวข้อง');
    }

    if ($('#select_masterplan_direction').val() == -1 || $('#select_masterplan_direction').val() == null) {
        _valid = false;
        $("#xyz_error_select_masterplan_direction").text('กรุณาเลือกแนวทางการพัฒนาภายใต้แผนย่อยที่เกี่ยวข้อง');
    }

    if ($('#select_y2_goal').val() == -1 || $('#select_y2_goal').val() == null) {
        _valid = false;
        $("#xyz_error_select_y2_goal").text('กรุณาเลือกเป้าหมายของแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่เกี่ยวข้อง');
    }

    if (is_value_chain) {
        if ($('#select_value_chain').val() == -1 || $('#select_value_chain').val() == null) {
            _valid = false;
            $("#xyz_error_select_value_chain").text('กรุณาเลือกองค์ประกอบที่เกี่ยวข้อง');
        }

        if ($('#select_factor').val() == -1 || $('#select_factor').val() == null) {
            _valid = false;
            $("#xyz_error_select_factor").text('กรุณาเลือกปัจจัยที่เกี่ยวข้อง');
        }
    }

    if (tree_z_3digits.getSelections().length == 0 && tree_target_z.getSelections().length == 0) {
        _valid = false;
        $("#xyz_error_tree_z_3digits").show();
        $("#xyz_error_tree_z_target").show();

    } else if (tree_z_3digits.getSelections().length == 0) {
        _valid = false;
        $("#xyz_error_tree_z_3digits").show();

    } else if (tree_target_z.getSelections().length == 0) {
        _valid = false;
        $("#xyz_error_tree_z_target").show();

    }

    var _check_dup = true;
    if (xyz_state != 'direct') {
        for (var i = 0; i < 6; i++) {

            if (xyz_selectbox_direct[i] != xyz_selectbox_secondary[i]) {
                _check_dup = false;
                break;
            }
        }

        if (_check_dup) {
            _valid = false;
            $("#xyz_error_select_masterplan_direction").text('กรุณาเลือกแนวทางการพัฒนา และเป้าหมายของแผนแม่บท ที่แตกต่างกับความสอดคล้องโดยตรง');
        }
    }

    return _valid;
}

function validate_xyz_step_2() {
    var _valid = true;
    $("#xyz_error_textbox_z_consistent").text('');

    if (!is_project_idea) {
        if ($('#textbox_z_consistent').val() == '') {
            _valid = false;
            $("#xyz_error_textbox_z_consistent").text('กรุณาอธิบายความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก');
        }
    }

    return _valid;
}

function validate_xyz_step_3() {
    var _valid = true;
    $("#xyz_error_textbox_y2_goal_consistent").text('');

    if (!is_project_idea) {
        if ($('#textbox_y2_goal_consistent').val() == '') {
            _valid = false;
            $("#xyz_error_textbox_y2_goal_consistent").text('กรุณาอธิบายความความสอดคล้องของโครงการกับเป้าหมายของแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่ทำการเลือก');
        }
    }

    return _valid;
}

function validate_xyz_step_4() {
    var _valid = true;
    $("#xyz_error_textbox_y1_goal_consistent").text('');
    $("#xyz_error_textbox_indicator_y1_contribution").text('');

    if ($('#textbox_y1_goal_consistent').val() == '') {
        _valid = false;
        $("#xyz_error_textbox_y1_goal_consistent").text('กรุณาอธิบายความความสอดคล้องของโครงการกับเป้าหมายของแผนย่อยที่ทำการเลือก');
    }

    return _valid;
}

function reset_validate_xyz_step_1() {
    $("#xyz_error_select_masterplan_title").text('');
    $("#xyz_error_select_masterplan_subplan").text('');
    $("#xyz_error_select_masterplan_goal").text('');
    $("#xyz_error_select_masterplan_direction").text('');
    $("#xyz_error_select_y2_goal").text('');

    $("#xyz_error_select_value_chain").text('');
    $("#xyz_error_select_factor").text('');

    $("#xyz_error_tree_z_3digits").hide();
    $("#xyz_error_tree_z_target").hide();
}