/* v1.2021.02.15.1 */

var result_emenscr_save = [];

var result_main_tag_document = [];

var state_result_tab1 = [];

var review_tab1_project_name = '';

function get_data_tab_1() {
    state_result_tab1 = [];

    let result_tab1_tree_planreform_main_dynamic_checked = tab1_tree_get_checked_node_leaf_list_save(tree_planreform_main_dynamic);
    let result_tab1_tree_planreform_option_checked = tab1_tree_get_checked_node_leaf_list_save(tree_planreform_option);
    let result_tab1_tree_plan12_checked = tab1_tree_get_checked_node_leaf_list_save(tree_plan12);
    let result_tab1_tree_homeland_policy_checked = g_homeland_policy_checked;
    let result_tab1_tree_homeland_policy_2020_checked = tab1_tree_get_checked_node_leaf_list_save(tree_homeland_policy);

    let result_tab1_tree_goverment_policy_main_checked = tab1_tree_get_checked_node_leaf_list_save_gov_policy(tree_government_policy);
    let result_tab1_governmentpolicies_priority_select_item = checkbox_group_get_check_item_list('#tab1_governmentpolicies_checkboxgroup');

    let result_tab1_ministry_plan = tab1_get_ministry_plan_list();
    let result_tab1_related_laws = tab1_get_related_laws_list();
    let result_tab1_government_ministers = tab1_get_government_ministers();

    review_tab1_project_name = $('#tab1_part1_textbox_project_name').val();

    state_result_tab1.push({
        "is_m1": true,
        'masterplan_direct': xyz_direct,
        'masterplan_secondary': xyz_secondary_arr,
        'government_policy_main_2019': result_tab1_tree_goverment_policy_main_checked,
        'government_policy_priority_2019': result_tab1_governmentpolicies_priority_select_item,
        'national_reform_direct': result_tab1_tree_planreform_main_dynamic_checked,
        'national_reform_secondary': result_tab1_tree_planreform_option_checked,
        'plan12': result_tab1_tree_plan12_checked,
        'national_security_plan': result_tab1_tree_homeland_policy_checked,
        'national_security_plan_2020': result_tab1_tree_homeland_policy_2020_checked,
        'ministry_plan': result_tab1_ministry_plan,
        'related_laws': result_tab1_related_laws,
        'related_government_ministers': result_tab1_government_ministers
    });

    console.log(state_result_tab1[0])
}

function form_full_save_data() {
    let result_emenscr_save = state_result_tab1[0];

    console.log(result_emenscr_save);
    console.log(JSON.stringify(result_emenscr_save));

    return JSON.stringify(result_emenscr_save);
}

function tab1_tree_get_checked_node_leaf_list(_tree) {
    if (_tree === null) {
        return [];
    }

    var _temp_tree_checked_node_list = [];
    var _tree_checked_node = _tree.getCheckedNodes();

    for (i = 0; i < _tree_checked_node.length; i++) {
        var _temp_node_data = _tree.getDataById(_tree_checked_node[i]);

        if (_temp_node_data._checkenabled == true) {

            if (_temp_node_data.kpi_id !== undefined) {
                _temp_tree_checked_node_list.push({
                    node_id: _temp_node_data.id,
                    node_name: _temp_node_data.name,
                    kpi_id: _temp_node_data.kpi_id
                });
            } else {
                _temp_tree_checked_node_list.push({
                    node_id: _temp_node_data.id,
                    node_name: _temp_node_data.name,
                    kpi_id: '0'
                });
            }
        }
    }

    return _temp_tree_checked_node_list;
}

function tab1_tree_get_checked_node_leaf_list_main_option(_tree_1, _tree_2) {
    var _temp_tree_checked_node_list = [];

    if (!(_tree_1 === null)) {
        var _tree_checked_node_1 = _tree_1.getCheckedNodes();

        for (i = 0; i < _tree_checked_node_1.length; i++) {
            var _temp_node_data = _tree_1.getDataById(_tree_checked_node_1[i]);

            if (_temp_node_data._checkenabled == true) {

                if (_temp_node_data.kpi_id !== undefined) {
                    _temp_tree_checked_node_list.push({
                        node_id: _temp_node_data.id,
                        node_name: _temp_node_data.name,
                        kpi_id: _temp_node_data.kpi_id
                    });
                }
            }
        }
    }

    if (!(_tree_2 === null)) {
        var _tree_checked_node = _tree_2.getCheckedNodes();

        for (i = 0; i < _tree_checked_node.length; i++) {
            var _temp_node_data_2 = _tree_2.getDataById(_tree_checked_node[i]);

            if (_temp_node_data_2._checkenabled == true) {

                if (_temp_node_data_2.kpi_id !== undefined) {
                    _temp_tree_checked_node_list.push({
                        node_id: _temp_node_data_2.id,
                        node_name: _temp_node_data_2.name,
                        kpi_id: _temp_node_data_2.kpi_id
                    });
                }
            }
        }
    }

    return _temp_tree_checked_node_list;
}

function tab1_tree_get_checked_node_leaf_list_save(_tree) {
    if (_tree === null) {
        return [];
    }

    var _temp_tree_checked_node_list = [];
    var _tree_checked_node = _tree.getCheckedNodes();

    for (i = 0; i < _tree_checked_node.length; i++) {
        var _temp_node_data = _tree.getDataById(_tree_checked_node[i]);

        if ((_temp_node_data._checkenabled == true)) {
            _temp_tree_checked_node_list.push(_temp_node_data.id);
        } else {
            if (_temp_node_data.hasOwnProperty('children')) {
                if (_temp_node_data.children.length == 0) {
                    _temp_tree_checked_node_list.push(_temp_node_data.id);
                }
            }
        }
    }

    return _temp_tree_checked_node_list;
}

function tab1_tree_get_checked_node_leaf_list_save_gov_policy(_tree) {
    if (_tree === null) {
        return [];
    }

    var _temp_tree_checked_node_list = [];
    var _tree_checked_node = _tree.getCheckedNodes();

    for (i = 0; i < _tree_checked_node.length; i++) {
        var _temp_node_data = _tree.getDataById(_tree_checked_node[i]);

        if ((_temp_node_data._checkenabled == true)) {
            _temp_tree_checked_node_list.push(_temp_node_data.id);
        }
    }

    return _temp_tree_checked_node_list;
}

function tab1_get_ministry_plan_list() {
    var _temp_ministry_plan = [];

    $('#tab1_ministry_plan_textbox_dynamic input[type=text]').each(function () {
        if ($(this).val().length > 0) {
            _temp_ministry_plan.push($(this).val());
        }
    });

    return _temp_ministry_plan;
}

function tab1_get_related_laws_list() {
    var _temp_related_laws = [];

    $('#tab1_related_laws_textbox_dynamic input[type=text]').each(function () {
        if ($(this).val().length > 0) {
            _temp_related_laws.push($(this).val());
        }
    });

    return _temp_related_laws;
}

function tab1_get_government_ministers() {
    var _temp_government_ministers = [];

    $('#tab1_related_government_ministers_textbox_dynamic input[type=text]').each(function () {
        if ($(this).val().length > 0) {
            _temp_government_ministers.push($(this).val());
        }
    });

    return _temp_government_ministers;
}

function tree_get_checked_node_list(_tree) {
    return _tree.getCheckedNodes();
}

function tree_get_json_by_list(_list) {
    var _list_obj = JSON.parse(_list);
    var _temp_json = [];

    for (var i = 1; i < _list_obj.length; i++) {
        _temp_json.push(_list_obj[i]['node_id']);
    }

    return _temp_json;
}

function checkbox_group_get_check_item_list(_checkbox_group_div_id) {
    var _temp_check_item = [];

    $(_checkbox_group_div_id).find("input:checked").each(function (i, ob) {
        _temp_check_item.push($(ob).val());
    });

    return _temp_check_item;
}

function get_bar_option_select_group(_bar_option_div_id) {
    // tab1_part1_main_option_group
    var _e_name = '#' + _bar_option_div_id;
    return $(_e_name).find('a.active').attr('data-title');
}

function get_bar_option_select_group_multiple_elements(_bar_option_div_id) {
    // tab1_part1_main_option_group
    var _e_name = '#' + _bar_option_div_id;
    var _list = $(_e_name).find('a.active').map(function () {
        return $(this).attr("data-title");
    }).get();
    var _temp_text = '';

    if (_list.length == 0) {
        _temp_text = 'ไม่มีข้อมูลวิธีการดำเนินงาน';
    } else {
        if (_list.length == 1) {
            _temp_text = _list[0];
        } else {
            _temp_text = _list[0] + ' และ' + _list[1];
        }
    }

    return _temp_text;
}