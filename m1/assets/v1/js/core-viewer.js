/* v1.2020.03.31.1 */

var json_tree_map_strategies = load_form_json('assets/treeview_jsons/v2/map_id_name/map_strategies_tree.json');
var json_tree_map_reform = load_form_json('assets/treeview_jsons/v2/map_id_name/map_reform_tree.json');
var json_tree_map_plan12 = load_form_json('assets/treeview_jsons/v2/map_id_name/map_plan12_tree.json');
var json_tree_map_homeland = load_form_json('assets/treeview_jsons/v2/map_id_name/map_homeland_policy_tree.json');

var json_tree_map_kpi_strategies = load_form_json('assets/treeview_jsons/v2/strategies/kpi_map/strategies.kpimap.json');
var json_tree_map_kpi_reform = load_form_json('assets/treeview_jsons/v2/planreform11/kpi_map/reform.kpimap.json');
var json_tree_map_kpi_plan12 = load_form_json('assets/treeview_jsons/v2/plan12/kpi_map/plan12.kpimap.json');
var json_tree_map_kpi_homeland = load_form_json('assets/treeview_jsons/v2/homeland/kpi_map/homeland_policy.kpimap.json');

var json_value_chain = null;

function viewer_rander_tab_1(_json_form, _viewpage) {

    json_value_chain = load_value_chain();

    if (typeof _json_form.document === 'undefined') {
        return;
    }

    let _json_form_tag_document = _json_form.document;

    let html_org = '';
    let html_strategic_goals = '';

    if (typeof _json_form_tag_document.national_strategic_goals !== 'undefined') {
        html_strategic_goals = rander_list_ulli('strategic_goals', _json_form_tag_document.national_strategic_goals);
        $('#viewer_span_tab1_strategies_goals').html(html_strategic_goals);

    }

    if (typeof _json_form_tag_document.masterplan_direct !== 'undefined') {
        if (typeof _json_form_tag_document.masterplan_direct.masterplan !== "undefined") {
            rander_xyz_main(_json_form_tag_document.masterplan_direct);
        }
    }

    if (typeof _json_form_tag_document.masterplan_secondary !== 'undefined') {
        if (_viewpage) {
            rander_xyz_secondary_view(_json_form_tag_document.masterplan_secondary);
        } else {
            rander_xyz_secondary(_json_form_tag_document.masterplan_secondary);
        }
    }

    if (typeof _json_form_tag_document.national_reform_direct !== 'undefined') {
        html_reform_direct = rander_tree_strategic('reform', _json_form_tag_document.national_reform_direct, json_tree_map_reform, 3);
        $('#viewer_span_tab2_national_reform_direct').html(html_reform_direct);
    }

    if (typeof _json_form_tag_document.national_reform_secondary !== 'undefined') {
        html_reform_option = rander_tree_strategic('reform', _json_form_tag_document.national_reform_secondary, json_tree_map_reform, 3);
        $('#viewer_span_tab2_national_reform_option').html(html_reform_option);
    }

    if (typeof _json_form_tag_document.plan12 !== 'undefined') {
        html_plan12 = rander_tree_strategic('plan12', _json_form_tag_document.plan12, json_tree_map_plan12, 3);
        $('#viewer_span_tab2_national_plan12').html(html_plan12);
    }

    if (typeof _json_form_tag_document.national_security_plan !== 'undefined') {
        html_homeland = rander_tree_strategic('homeland', _json_form_tag_document.national_security_plan, json_tree_map_homeland, 4);
        $('#viewer_span_tab2_national_homeland_policy').html(html_homeland);
    }

    if (typeof _json_form_tag_document.national_security_plan_2020 !== 'undefined') {
        html_homeland = rander_tree_strategic('homeland', _json_form_tag_document.national_security_plan_2020, json_tree_map_homeland, 4);
        $('#viewer_span_tab2_national_homeland_policy').html(html_homeland);
    }

    if (typeof _json_form_tag_document.ministry_plan !== 'undefined') {
        html_ministry_plan = rander_list_ulli('ulli', _json_form_tag_document.ministry_plan);
        $('#viewer_span_tab2_national_ministry_plan').html(html_ministry_plan);
    }

    if (typeof _json_form_tag_document.government_policy_main_2019 !== 'undefined') {
        html_governmentpolicies_main_check = rander_list_ulli('governmentpolicies_main', _json_form_tag_document.government_policy_main_2019);
        $('#viewer_span_tab2_governmentpolicies_main_check').html(html_governmentpolicies_main_check);
    }

    if (typeof _json_form_tag_document.government_policy_priority_2019 !== 'undefined') {
        html_governmentpolicies_priority_check = rander_list_ulli('governmentpolicies', _json_form_tag_document.government_policy_priority_2019);
        $('#viewer_span_tab2_governmentpolicies_priority_check').html(html_governmentpolicies_priority_check);
    }

    if (typeof _json_form_tag_document.related_laws !== 'undefined') {
        html_related_laws = rander_list_ulli('ulli', _json_form_tag_document.related_laws);
        $('#viewer_span_tab2_related_laws').html(html_related_laws);
    }

    if (typeof _json_form_tag_document.related_government_ministers !== 'undefined') {
        html_related_government_ministers = rander_list_ulli('ulli', _json_form_tag_document.related_government_ministers);
        $('#viewer_span_tab2_related_government_ministers').html(html_related_government_ministers);
    }

    if (typeof _json_form_tag_document.org_l3 !== 'undefined') {
        html_org += _json_form_tag_document.org_l3 + ' ';
    }

    if (typeof _json_form_tag_document.org_l2 !== 'undefined') {
        html_org += _json_form_tag_document.org_l2 + ' ';
    }

    if (typeof _json_form_tag_document.org_l1 !== 'undefined') {
        html_org += _json_form_tag_document.org_l1;
    }

    if (typeof _json_form_tag_document.name !== 'undefined') {
        $('#viewer_span_header_project_name').text(_json_form_tag_document.name);
    }

    $('#viewer_span_header_org').text(html_org);

}


function rander_xyz_main(_xyz_template) {
    let _html_z_direct = rander_tree_strategic('strategies', [_xyz_template.z_3digits], json_tree_map_strategies, 4);
    let _html_z_target = rander_tree_strategic('strategies', [_xyz_template.z_goal], json_tree_map_strategies, 4);

    let _y2_header = _xyz_template.y2_goal.substr(0, 2) + '00';
    $('#viewer_span_tab1_header_direct').text('M.1 แนวทางการพัฒนาของแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่เกี่ยวข้องโดยตรง: ' + get_xyz_text_byID(_y2_header));

    $('#viewer_span_tab1_z_direct').html(_html_z_direct);
    $('#viewer_span_tab1_z_target').html(_html_z_target);
    $('#viewer_span_tab1_z_consistent').text(_xyz_template.report_z_consistent);

    $('#viewer_span_tab1_y2_header').text(get_xyz_text_byID(_xyz_template.y2_goal.substr(0, 4)));
    $('#viewer_span_tab1_y2_goal').text(get_xyz_text_byID(_xyz_template.y2_goal));
    $('#viewer_span_tab1_y2_goal_consistent').text(_xyz_template.report_y2_goal.consistent);
    $('#viewer_span_tab1_y2_goal_indicators').html(create_table_y_indicator(_xyz_template.report_y2_goal.indicators));

    $('#viewer_span_tab1_y1_header').text(get_xyz_text_byID(_xyz_template.y1_goal.substr(0, 4)));
    $('#viewer_span_tab1_masterplan').text(get_xyz_text_byID(_xyz_template.y1_direction));
    $('#viewer_span_tab1_y1_goal').text(get_xyz_text_byID(_xyz_template.y1_goal));
    $('#viewer_span_tab1_y1_goal_consistent').text(_xyz_template.report_y1_goal.consistent);
    $('#viewer_span_tab1_y1_goal_indicators').html(create_table_y_indicator(_xyz_template.report_y1_goal.indicators));

    if (typeof _xyz_template.value_chain != 'undefined') {

        let xyz_value_chain = get_value_chain_text_byID(_xyz_template.value_chain, _xyz_template.factor)

        $('#viewer_span_tab1_value_chain').text(`${xyz_value_chain.value_chain} (${_xyz_template.value_chain})`);
        $('#viewer_span_tab1_factor').text(`${xyz_value_chain.factor} (${_xyz_template.factor})`);
    }
}

function rander_xyz_secondary(_xyz_template) {
    $('#section1_master_secondary').empty();

    for (let i = 0; i < _xyz_template.length; i++) {
        let _html_z_direct = rander_tree_strategic('strategies', [_xyz_template[i].z_3digits], json_tree_map_strategies, 4);
        let _html_z_target = rander_tree_strategic('strategies', [_xyz_template[i].z_goal], json_tree_map_strategies, 4);

        let _y2_header = _xyz_template[i].y2_goal.substr(0, 2) + '00';


        let _html = `
        <div id="section1_option">
            <article class="timeline-entry" id="view-type-1">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success" id="report_panel_status_1">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                     </div>
                    <div class="timeline-label timeline-label-default">
                        <div class="panel panel-primary" id="report_panel_header_1">
                            <div id="viewer_span_tab1_header_direct" class="panel-heading">
                                <i class="fa fa-th-list"></i>M.1 แนวทางการพัฒนาของแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่เกี่ยวข้องในระดับรอง:
                                 ${get_xyz_text_byID(_y2_header)}
                            </div>
                            <div class="panel-body no-padding" id="report_panel_content_1">
                                <table id="w0" class="table detail-view">
                                    <thead>
                                        <tr><th colspan="3" class="text-center">สรุปความสอดคล้องของโครงการกับยุทธศาสตร์ชาติ</th></tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก</th>
                                            <td>
                                                <span id="viewer_span_tab1_z_direct">${_html_z_direct}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เป้าหมายของยุทธศาสตร์ชาติ</th>
                                            <td>
                                                <span id="viewer_span_tab1_z_target">${_html_z_target}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>อธิบายความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก</th>
                                            <td>
                                                <span id="viewer_span_tab1_z_consistent">${_xyz_template[i].report_z_consistent}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="text-center">สรุปความสอดคล้องของโครงการกับเป้าหมายและตัวชี้วัดของแผนแม่บทประเด็น</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>แผนแม่บทภายใต้ยุทธศาสตร์ชาติ</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_header">${get_xyz_text_byID(_xyz_template[i].y2_goal.substr(0, 4))}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เป้าหมาย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_goal">${get_xyz_text_byID(_xyz_template[i].y2_goal)}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ความสอดคล้องของโครงการกับเป้าหมายของแผนแม่บทประเด็น</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_goal_consistent">${_xyz_template[i].report_y2_goal.consistent}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ตัวชี้วัดของแผนแม่บทประเด็น</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_goal_indicators">${create_table_y_indicator(_xyz_template[i].report_y2_goal.indicators)}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="text-center">สรุปความสอดคล้องของโครงการกับเป้าหมายและตัวชี้วัดของแผนย่อย</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>แผนย่อย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_header">${get_xyz_text_byID(_xyz_template[i].y1_goal.substr(0, 4))}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>แนวทางการพัฒนา</th>
                                            <td>
                                                <span id="viewer_span_tab1_masterplan">${get_xyz_text_byID(_xyz_template[i].y1_direction)}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เป้าหมาย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_goal">${get_xyz_text_byID(_xyz_template[i].y1_goal)}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_goal_consistent">${_xyz_template[i].report_y1_goal.consistent}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ตัวชี้วัดของแผนย่อย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_goal_indicators">${create_table_y_indicator(_xyz_template[i].report_y1_goal.indicators)}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        `;

        $('#section1_master_secondary').append(_html);
    }
}

function rander_xyz_secondary_view(_xyz_template) {
    $('#section1_master_secondary').empty();

    for (let i = 0; i < _xyz_template.length; i++) {
        let _html_z_direct = rander_tree_strategic('strategies', [_xyz_template[i].z_3digits], json_tree_map_strategies, 4);
        let _html_z_target = rander_tree_strategic('strategies', [_xyz_template[i].z_goal], json_tree_map_strategies, 4);

        let _y2_header = _xyz_template[i].y2_goal.substr(0, 2) + '00';


        let _html = `
        <div id="section1_option">
            <article class="timeline-entry" id="view-type-1">
                <div class="timeline-entry-inner">
                    <div class="timeline-label timeline-label-primary">
                        <div class="card" id="report_panel_header_2">
                            <div id="viewer_span_tab1_header_direct" class="card-header bg-secondary text-white">
                                M.1 แนวทางการพัฒนาของแผนแม่บทภายใต้ยุทธศาสตร์ชาติที่เกี่ยวข้องในระดับรอง: ${get_xyz_text_byID(_y2_header)}
                            </div>
                            <div class="panel-body no-padding" id="report_panel_content_1">
                                <table id="w0" class="table detail-view">
                                    <thead>
                                        <tr><th colspan="3" class="text-center">สรุปความสอดคล้องของโครงการกับยุทธศาสตร์ชาติ</th></tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก</th>
                                            <td>
                                                <span id="viewer_span_tab1_z_direct">${_html_z_direct}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เป้าหมายของยุทธศาสตร์ชาติ</th>
                                            <td>
                                                <span id="viewer_span_tab1_z_target">${_html_z_target}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>อธิบายความสอดคล้องของโครงการกับยุทธศาสตร์ชาติที่ทำการเลือก</th>
                                            <td>
                                                <span id="viewer_span_tab1_z_consistent">${_xyz_template[i].report_z_consistent}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="text-center">สรุปความสอดคล้องของโครงการกับเป้าหมายและตัวชี้วัดของแผนแม่บทประเด็น</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>แผนแม่บทภายใต้ยุทธศาสตร์ชาติ</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_header">${get_xyz_text_byID(_xyz_template[i].y2_goal.substr(0, 4))}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เป้าหมาย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_goal">${get_xyz_text_byID(_xyz_template[i].y2_goal)}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ความสอดคล้องของโครงการกับเป้าหมายของแผนแม่บทประเด็น
                                            </th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_goal_consistent">${_xyz_template[i].report_y2_goal.consistent}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ตัวชี้วัดของแผนแม่บทประเด็น</th>
                                            <td>
                                                <span id="viewer_span_tab1_y2_goal_indicators">${create_table_y_indicator(_xyz_template[i].report_y2_goal.indicators)}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="text-center">สรุปความสอดคล้องของโครงการกับเป้าหมายและตัวชี้วัดของแผนย่อย</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>แผนย่อย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_header">${get_xyz_text_byID(_xyz_template[i].y1_goal.substr(0, 4))}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>แนวทางการพัฒนา</th>
                                            <td>
                                                <span id="viewer_span_tab1_masterplan">${get_xyz_text_byID(_xyz_template[i].y1_direction)}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เป้าหมาย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_goal">${get_xyz_text_byID(_xyz_template[i].y1_goal)}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ความสอดคล้องของโครงการกับเป้าหมายของแผนย่อย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_goal_consistent">${_xyz_template[i].report_y1_goal.consistent}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ตัวชี้วัดของแผนย่อย</th>
                                            <td>
                                                <span id="viewer_span_tab1_y1_goal_indicators">${create_table_y_indicator(_xyz_template[i].report_y1_goal.indicators)}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        `;

        $('#section1_master_secondary').append(_html);
    }
}


function rander_tree_strategic(_type, _json_form_tag_document_tree, _json_tree_map, _idx_code) {
    let _temp_tree_sub_text = '';

    if (_json_form_tag_document_tree.length > 0) {
        let _arr_strategic_direct_split = [];

        for (let i = 0; i < _json_form_tag_document_tree.length; i++) {
            let _temp_sd = _json_form_tag_document_tree[i];

            _arr_strategic_direct_split = [];
            _arr_strategic_direct_split.push(_temp_sd.substring(0, _idx_code));

            for (let j = _idx_code; j < _temp_sd.length; j += 2) {
                let _a = _arr_strategic_direct_split[_arr_strategic_direct_split.length - 1] + _temp_sd.substring(j, j + 2);
                _arr_strategic_direct_split.push(_a);
            }

            _temp_tree_sub_text += tree_get_name(_type, _json_tree_map, _arr_strategic_direct_split[0]);

            for (let k = 1; k < _arr_strategic_direct_split.length; k++) {
                if (k == 1) {
                    _temp_tree_sub_text += `<ul class="tree"><li>${tree_get_name(_type, _json_tree_map, _arr_strategic_direct_split[k])}`;

                } else if (k == _arr_strategic_direct_split.length - 1) {
                    _temp_tree_sub_text += `<ul><li><b>${tree_get_name(_type, _json_tree_map, _arr_strategic_direct_split[k])}`;

                } else {
                    _temp_tree_sub_text += `<ul><li>${tree_get_name(_type, _json_tree_map, _arr_strategic_direct_split[k])}`;

                }
            }

            for (let k = 1; k < _arr_strategic_direct_split.length; k++) {
                if (k == 1) {
                    _temp_tree_sub_text += '</b></li></ul>';
                } else {
                    _temp_tree_sub_text += '</li></ul>';
                }
            }

            _temp_tree_sub_text += '</ul>';
            _temp_tree_sub_text += '<br>';
        }
    }

    return _temp_tree_sub_text;
}

function rander_list_ulli(_type, _json_form_tag_document_list) {

    if (_json_form_tag_document_list === undefined) {
        return '';
    }

    let _html_content = '';

    if (_json_form_tag_document_list.length > 0) {
        if (_type == 'ulli') {
            _html_content += '<ul>';
        }

        for (let i = 0; i < _json_form_tag_document_list.length; i++) {
            let _a = '';
            if (_type == 'strategic_goals') {
                _a = get_national_strategic_goals_by_index(_json_form_tag_document_list[i]);
                _html_content += `<span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> ${_a}<br>`;

            } else if (_type == 'governmentpolicies') {
                _a = get_governmentpolicies_check_by_index(_json_form_tag_document_list[i]);
                _html_content += `<i class="mdi mdi-check-circle"></i> ${_a}<br>`;

            } else if (_type == 'governmentpolicies_main') {
                //_a = tree_government_policy.getDataById(_json_form_tag_document_list[i]).name;
                _a = get_gov_policy_byID(_json_form_tag_document_list[i]);
                _html_content += `<i class="mdi mdi-check-circle"></i> ${_a}<br>`;

            } else {
                _a = _json_form_tag_document_list[i];
                _html_content += `<li> ${_a} </li>`;
            }
        }

        if (_type == 'ulli') {
            _html_content += '</ul>';
        }

    }

    return _html_content;
}

function rander_progress_tree(_type, _json_form_tag_progress_sub) {
    let _temp_progress_text = '';

    if (_json_form_tag_progress_sub.length > 0) {

        for (let i = 0; i < _json_form_tag_progress_sub.length; i++) {

            if (_type == 'strategic_direct') {
                _temp_progress_text += `<strong>
                                            <span class="report_header">ผลการดำเนินงานต่อเป้าหมายและตัวชี้วัดของแผนยุทธศาสตร์ชาติ ${tree_get_topic_progress('strategies', _json_form_tag_progress_sub[i].node_id)}</span>
                                        </strong><br><br>`;

            } else if (_type == 'reform_direct') {
                _temp_progress_text += `<strong><span class="report_header">ผลการดำเนินงานต่อเป้าหมายและตัวชี้วัดแผนปฏิรูปประเทศ ${tree_get_topic_progress('reform', _json_form_tag_progress_sub[i].node_id)}</span>
                                        </strong><br><br>`;

            } else if (_type == 'plan12') {
                _temp_progress_text += `<strong><span class="report_header">ผลการดำเนินงานต่อเป้าหมายและตัวชี้วัดลำดับที่ ${(parseInt(i) + 1)}</span>
                                        </strong><br><br>`;

            } else if (_type == 'homeland') {
                _temp_progress_text += `<strong><span class="report_header">ผลการดำเนินงานต่อเป้าหมายและตัวชี้วัดลำดับที่ ${(parseInt(i) + 1)}</span>
                                        </strong><br><br>`;

            }

            _temp_progress_text += '<table class="table table-condensed"><thead><tr><th style="width: 50%">เป้าหมาย</th><th style="width: 50%">ผลการดำเนินงานต่อเป้าหมาย</th></tr></thead><tbody>';

            _temp_progress_text += '<tr><td>';

            if (_type == 'strategic_direct') {
                _temp_progress_text += tree_get_name_strategies(json_tree_map_strategies, _json_form_tag_progress_sub[i].node_id);

            } else if (_type == 'reform_direct') {
                _temp_progress_text += tree_get_name_reform(json_tree_map_reform, _json_form_tag_progress_sub[i].node_id);

            } else if (_type == 'plan12') {
                _temp_progress_text += tree_get_name_no_header(json_tree_map_plan12, _json_form_tag_progress_sub[i].node_id);

            } else if (_type == 'homeland') {
                _temp_progress_text += tree_get_name_no_header(json_tree_map_homeland, _json_form_tag_progress_sub[i].node_id);
            } else {

            }

            _temp_progress_text += '</td><td>';
            _temp_progress_text += _json_form_tag_progress_sub[i].progress_report + '<br>';
            _temp_progress_text += '</td></tr>';
            _temp_progress_text += '</tbody></table>';

            if (_json_form_tag_progress_sub[i].kpi.length > 0) {

                _temp_progress_text += '<table class="table table-condensed"><thead><tr><th style="width: 50%">ตัวชี้วัด</th><th style="width: 50%">ผลการดำเนินงานต่อตัวชี้วัด</th></tr></thead><tbody>';

                for (let j = 0; j < _json_form_tag_progress_sub[i].kpi.length; j++) {
                    let _temp_kpi = '';

                    if (_type == 'strategic_direct') {
                        _temp_kpi = report_get_kpi_detail_by_id(json_tree_map_kpi_strategies, _json_form_tag_progress_sub[i].kpi[j].kpi_id);

                    } else if (_type == 'reform_direct') {
                        _temp_kpi = report_get_kpi_detail_by_id(json_tree_map_kpi_reform, _json_form_tag_progress_sub[i].kpi[j].kpi_id);

                    } else if (_type == 'plan12') {
                        _temp_kpi = report_get_kpi_detail_by_id_no_header(json_tree_map_kpi_plan12, _json_form_tag_progress_sub[i].kpi[j].kpi_id);

                    } else if (_type == 'homeland') {
                        _temp_kpi = report_get_kpi_detail_by_id_no_header_substring(json_tree_map_kpi_homeland, _json_form_tag_progress_sub[i].kpi[j].kpi_id);

                    } else {

                    }

                    _temp_progress_text += '<tr><td>';
                    _temp_progress_text += _temp_kpi;
                    _temp_progress_text += '</td><td>';
                    _temp_progress_text += _json_form_tag_progress_sub[i].kpi[j].progress_report + '<br>';
                    _temp_progress_text += '</td></tr>';

                }
                _temp_progress_text += '</tbody></table>';
            }


            if (i < _json_form_tag_progress_sub.length - 1) {
                _temp_progress_text += '<hr>';
            }

        }

    }

    return _temp_progress_text;
}

function tree_get_name(_type, _json_tree_map, _node_id) {
    if (_type == 'strategies') {
        return tree_get_name_strategies(_json_tree_map, _node_id);
    } else if (_type == 'plan12') {
        return tree_get_name_no_header(_json_tree_map, _node_id);
    } else if (_type == 'reform') {
        return tree_get_name_reform(_json_tree_map, _node_id);
    } else if (_type == 'homeland') {
        return tree_get_name_no_header(_json_tree_map, _node_id);
    } else {
        return 'ไม่พบข้อมูล';
    }

}

function tree_get_name_strategies(_json_tree_map_strategies, _node_id) {
    if (_node_id.length == 0) {
        return [];
    }

    let sub_node_id = _node_id.substring(0, 3);
    let _idx = 0;

    if (sub_node_id == 'Y10') {
        _idx = 0;
    } else if (sub_node_id == 'Y20') {
        _idx = 1;
    } else if (sub_node_id == 'Y30') {
        _idx = 2;
    } else if (sub_node_id == 'Y40') {
        _idx = 3;
    } else if (sub_node_id == 'Y50') {
        _idx = 4;
    } else if (sub_node_id == 'Y60') {
        _idx = 5;
    } else {
        _idx = 0;
    }

    _temp_result = json_get_tree_name_by_id(_json_tree_map_strategies[_idx].nodes, _node_id);

    if (_temp_result.length > 0) {
        return [_temp_result[0].name];
    }

    return [];
}

function tree_get_name_reform(_json_tree_map_strategies, _node_id) {
    if (_node_id.length == 0) {
        return [];
    }

    let sub_node_id = _node_id.substring(0, 3);
    let _idx = 0;

    if (sub_node_id == 'A01') {
        _idx = 0;
    } else if (sub_node_id == 'B01') {
        _idx = 1;
    } else if (sub_node_id == 'C01') {
        _idx = 2;
    } else if (sub_node_id == 'D01') {
        _idx = 3;
    } else if (sub_node_id == 'E01') {
        _idx = 4;
    } else if (sub_node_id == 'F01') {
        _idx = 5;
    } else if (sub_node_id == 'G01') {
        _idx = 6;
    } else if (sub_node_id == 'H01') {
        _idx = 7;
    } else if (sub_node_id == 'I01') {
        _idx = 8;
    } else if (sub_node_id == 'J01') {
        _idx = 9;
    } else if (sub_node_id == 'K01') {
        _idx = 10;
    } else if (sub_node_id == 'M01') {
        _idx = 11;
    } else {
        _idx = 0;
    }

    _temp_result = json_get_tree_name_by_id(_json_tree_map_strategies[_idx].nodes, _node_id);

    if (_temp_result.length > 0) {
        return [_temp_result[0].name];
    }

    return [];
}

function tree_get_name_no_header(_json_tree_map, _node_id) {
    if (_node_id.length == 0) {
        return [];
    }

    _temp_result = json_get_tree_name_by_id(_json_tree_map, _node_id);

    if (_temp_result.length > 0) {
        return [_temp_result[0].name];
    }

    return [];
}

function report_get_kpi_detail_by_id(_kpi_list_json, _kpi_id) {
    // Remove last character in string
    let temp_kpi_ = '';

    if (_kpi_id === null) {
        return '';
    }

    if (_kpi_id.length == 0) {
        return '';
    }

    _kpi_id_sub = _kpi_id.substring(0, _kpi_id.length - 2);

    for (let i = 0; i < _kpi_list_json.length; i++) {
        if (_kpi_list_json[i].hasOwnProperty(_kpi_id_sub)) {
            if (_kpi_list_json[i][_kpi_id_sub].hasOwnProperty(_kpi_id)) {

                return _kpi_list_json[i][_kpi_id_sub][_kpi_id];
            }
        }
    }

    return '';
}

function report_get_kpi_detail_by_id_no_header(_kpi_list_json, _kpi_id) {
    // Remove last character in string
    let temp_kpi_ = '';

    if (_kpi_id === null) {
        return '';
    }

    if (_kpi_id.length == 0) {
        return '';
    }

    if (_kpi_list_json.hasOwnProperty(_kpi_id)) {
        if (_kpi_list_json[_kpi_id].hasOwnProperty(_kpi_id)) {
            return _kpi_list_json[_kpi_id][_kpi_id];
        }
    }

    return '';
}

function report_get_kpi_detail_by_id_no_header_substring(_kpi_list_json, _kpi_id) {
    // Remove last character in string
    let temp_kpi_ = '';

    if (_kpi_id === null) {
        return '';
    }

    if (_kpi_id.length == 0) {
        return '';
    }

    _kpi_id_sub = _kpi_id.substring(0, _kpi_id.length - 2);

    if (_kpi_list_json.hasOwnProperty(_kpi_id_sub)) {
        if (_kpi_list_json[_kpi_id_sub].hasOwnProperty(_kpi_id)) {
            return _kpi_list_json[_kpi_id_sub][_kpi_id];
        }
    }

    return '';
}

function get_national_strategic_goals_by_index(_idx) {
    let _strategic_goals = ['ประเทศชาติมีความมั่นคง ประชาชนมีความสุข',
        'การยกระดับศักยภาพในหลากหลายมิติเพื่อการพัฒนาเศรษฐกิจอย่างต่อเนื่อง',
        'มีการพัฒนาคนในทุกมิติเเละในทุกช่วงวัยให้เป็นคนด เก่ง และมีคุณภาพ',
        'มีการสร้างโอกาสและความเสมอภาคทางสังคม',
        'มีการสร้างการเติบโตบนคุณภาพชีวิตที่เป็นมิตรกับสิ่งเเวดล้อม',
        'มีภาครัฐของประชาชนเพื่อประชาชนและประโยชน์ส่วนรวม'
    ];

    return _strategic_goals[_idx];
}

function get_governmentpolicies_check_by_index(_idx) {
    let governmentpolicies_check = ['การแก้ใขปัญหาในการดำรงชีวิตของประชาชน',
        'การปรับปรุงระบบสวัสดิการและพัฒนาคุณภาพชีวิตของประชาชน',
        'มาตรการเศรษฐกิจเพื่อรองรับความผันผวนของเศรษฐกิจโลก',
        'การให้ความช่วยเหลือเกษตรกรและพัฒนานวัตกรรม',
        'การยกระดับศักยภาพของแรงงาน',
        'การวางรากฐานระบบเศรษฐกิจของประเทศสู่อนาคต',
        'การเตรียมคนไทยสู่ศตวรรษที่ ๒๑',
        'การแก้ไขปัญหาทุจริตและประพฤติมิชอบในวงราชการทั้งฝ่ายการเมืองและฝ่ายราชการประจำ',
        'การแก้ไขปัญหายาเสพติดและสร้างความสงบสุขในพื้นที่ชายแดนภาคใต้',
        'การพัฒนาระบบการให้บริการประชาชน',
        'การจัดเตรียมมาตรการรองรับภัยแล้งและอุทกภัย',
        'การสนับสนุนให้มีการศึกษา การรับฟังความเห็นของประชาชน และการดำเนินการเพื่อแก้ไขเพิ่มเติมรัฐธรรมนูญ',
    ];

    return governmentpolicies_check[_idx];
}

function tree_get_topic_progress(_type, _node_id) {

    if (_type == 'strategies') {
        let sub_node_id = _node_id.substring(0, 3);

        if (sub_node_id == 'Y10') {
            return 'ด้านความมั่นคง';
        } else if (sub_node_id == 'Y20') {
            return 'ด้านการสร้างความสามารถในการแข่งขัน';
        } else if (sub_node_id == 'Y30') {
            return 'ด้านการพัฒนาและเสริมสร้างศักยภาพมนุษย์';
        } else if (sub_node_id == 'Y40') {
            return 'ด้านการสร้างโอกาสและความเสมอภาคทางสังคม';
        } else if (sub_node_id == 'Y50') {
            return 'ด้านการสร้างการเติบโตบนคุณภาพชีวิตที่เป็นมิตรต่อสิ่งแวดล้อม';
        } else if (sub_node_id == 'Y60') {
            return 'ด้านการปรับสมดุลและพัฒนาระบบการบริหารจัดการภาครัฐ';
        } else {
            return '';
        }

    } else if (_type == 'plan12') {
        return tree_get_name_no_header(_json_tree_map, _node_id);

    } else if (_type == 'reform') {
        let sub_node_id = _node_id.substring(0, 1);

        if (sub_node_id == 'A') {
            return 'ด้านการป้องกันและปราบปรามการทุจริตและประพฤติมิชอบ';
        } else if (sub_node_id == 'B') {
            return 'ด้านเศรษฐกิจ';
        } else if (sub_node_id == 'C') {
            return 'ด้านทรัพยากรและสิ่งแวดล้อม';
        } else if (sub_node_id == 'D') {
            return 'ด้านพลังงาน';
        } else if (sub_node_id == 'E') {
            return 'ด้านการบริหารราชการแผ่นดิน';
        } else if (sub_node_id == 'F') {
            return 'ด้านสาธารณสุข';
        } else if (sub_node_id == 'G') {
            return 'กระบวนการยุติธรรม';
        } else if (sub_node_id == 'H') {
            return 'ด้านกฎหมาย';
        } else if (sub_node_id == 'I') {
            return 'ด้านสื่อสารมวลชน เทคโนโลยีสารสนเทศ';
        } else if (sub_node_id == 'J') {
            return 'ด้านการเมือง';
        } else if (sub_node_id == 'K') {
            return 'ด้านการปฏิรูปประเทศด้านสังคม';
        } else {
            return '';
        }

    } else if (_type == 'homeland') {
        return tree_get_name_no_header(_json_tree_map, _node_id);
    } else {
        return '';
    }
}

function json_get_tree_name_by_id(_json, _id) {
    return _json.filter(
        function (_json) {
            return _json.id == _id;
        }
    );
}

function report_quarterLabel(date) {
    let config_quarter_labels = "Q ";
    let month = date.getMonth();
    let q_num;

    if (month >= 9) {
        q_num = 4;
    } else if (month >= 6) {
        q_num = 3;
    } else if (month >= 3) {
        q_num = 2;
    } else {
        q_num = 1;
    }

    return config_quarter_labels + q_num;
}

function get_gov_policy_byID(_id) {
    if (gov_policy_kv[_id] === undefined) {
        return -1;
    } else {
        return gov_policy_kv[_id];
    }
}

function numberWithCommas(x) {
    if (x === undefined || x.length == 0) {
        return '0';
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

}

function remove_number_commas(x) {
    if (x === undefined || x.length == 0) {
        return '';
    } else {
        if (typeof x === 'string' || x instanceof String) {
            return parseFloat(x.replace(/,/g, ''));
        } else {
            return parseFloat(x);
        }
    }

}

function convert_date_to_thai(_d) {
    if (_d.length == 0) {
        return '';
    }

    let _thai_months = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
        "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
    ];
    _d_split = _d.trim().split('-');

    if (_d_split[1] < 1 || _d_split[1] > 12) {
        return '';
    }

    return _thai_months[_d_split[1] - 1] + ' พ.ศ. ' + _d_split[0];
}

function load_form_json(_json_file) {
    let _read_form = JSON.parse($.getJSON({
        'url': _json_file,
        'async': false
    }).responseText);

    return _read_form;
}

function rander_xyz_secondary_progress(_xyz) {
    let _html = '';

    if (typeof _xyz != 'undefined') {

        for (let i = 0; i < _xyz.length; i++) {
            _html += `
            <tr>
                <th colspan="2" style="text-align: center; vertical-align: middle;">ประเด็นที่ ${(i + 1)}</th>
            </tr>
            <tr>
                <th>ผลการดำเนินงานต่อเป้าหมายของยุทธศาสตร์ชาติ</th>
                <td><span>${_xyz[i].z_goal}</span></td>
            </tr>
            <tr>
                <th>ผลการดำเนินงานต่อเป้าหมายของแผนแม่บทประเด็น</th>
                <td><span>${_xyz[i].y2_goal}</span></td>
            </tr>
            <tr>
                <th>ผลการดำเนินงานต่อเป้าหมายของแผนย่อย</th>
                <td><span>${_xyz[i].y1_goal}</span></td>
            </tr>
        `;
        }
    }

    return _html;
}