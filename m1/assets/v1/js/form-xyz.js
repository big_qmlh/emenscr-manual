/* v1.2019.10.30.1 */

var tree_goal_z = null;
var tree_target_z = null;
//var tree_plan_z = null;

var xyz_state = 'direct';
var xyz_state_remove = 'direct';

var xyz_template = {};
var xyz_direct = {};
var xyz_selectbox_direct = {};
var xyz_selectbox_secondary = {};

var xyz_secondary = {};
var xyz_secondary_arr = [];
var xyz_secondary_arr_temp = [];
var xyz = {};

var next_page_check = '';

$('#main_modal_select_masterplan li a').click(function (e) {
    if ($(this).attr('id') == 'a_tab_step_1') {
        return true;
    }

    if (next_page_check != $(this).attr('id')) {
        e.preventDefault();
        return false;
    }
});


$(document).ready(function () {

    $('#xyz_table_primary').hide();
    $('#xyz_table_group_secondary').hide();

    //$('#main_modal_select_masterplan').modal('show');
    //$('#main_modal_preview_masterplan').modal('show');

    //$("#btn_add_xyz_direct").click();

    $('.nav-tabs > li a[title]').tooltip();

});

$("body").tooltip({
    selector: '[data-toggle="tooltip"]'
});

$("#btn_add_xyz_direct").on("click", function (e) {
    xyz_state = 'direct';

    xyz_init_xyz();
    $('#main_modal_select_masterplan').modal('show');
    $("#modal_masterplan_step1").addClass("active in");
});

$("#btn_add_xyz_second").on("click", function (e) {
    xyz_state = 'secondary';

    xyz_init_xyz();
    $('#main_modal_select_masterplan').modal('show');
    $("#modal_masterplan_step1").addClass("active in");
});

$(document).on("click", '[id^=xyz_secondary_remove_]', function () {
    secondary_current_remove_id = $(this);
});


$("#btn_xyz_delete").on("click", function (e) {
    if (xyz_state_remove == 'direct') {
        xyz_template = {};
        xyz_direct = {};

        $('#xyz_table_primary').hide();
        $('#btn_add_xyz_direct').show();
        $('#xyz_confirm_delete').modal('hide');

        $('#div_info_strategies').show();
        $('#div_block_strategies_direct').hide();
        $('#block_xyz_option').hide();

        $('#nav_pills_tab_header_strategies').addClass('nav_pills_tab_disable');
        $('#nav_pills_tab_header_strategies').attr('data-toggle', 'tooltip');
        $('#nav_pills_tab_header_strategies').attr('title', 'ยุทธศาสตร์ชาติจะถูกเชื่อมโยงผ่านแผนแม่บทภายใต้ยุทธศาสตร์ชาติให้โดยอัตโนมัติ กรุณาเลือกแผนแม่บทภายใต้ยุทธศาสตร์ชาติด้านล่าง');

    } else {
        var _index = get_array_index_bykey(xyz_secondary_arr, xyz_secondary.id);

        xyz_secondary_arr.splice(_index, 1);

        if (xyz_secondary_arr.length > 0) {
            $('#div_block_strategies_option').show();
            $('#xyz_table_group_secondary').show();
        } else {
            $('#div_block_strategies_option').hide();
            $('#xyz_table_group_secondary').hide();
        }

        var _temp_z_3digits = [];
        var _temp_z_goal = [];

        for (var i = 0; i < xyz_secondary_arr.length; i++) {
            _temp_z_3digits.push(xyz_secondary_arr[i].z_3digits);
            _temp_z_goal.push(xyz_secondary_arr[i].z_goal);
        }

        auto_check_tree_strategies_secondary(_temp_z_3digits);

        xyz_template = {};
        xyz_secondary = {};

        secondary_current_remove_id.parents("tr").remove();
        $('#xyz_confirm_delete').modal('hide');
    }
});

$('#select_masterplan_title').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        xyz['s1_masterplan_title'] = _id.substring(0, 2);
        get_masterplan_subplab(xyz['s1_masterplan_title']);

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['1'] = xyz['s1_masterplan_title'];
        } else {
            xyz_selectbox_secondary['1'] = xyz['s1_masterplan_title'];
        }
    }

    $('#row_z_block').hide();

    //run_all_combination();
});

$('#select_masterplan_subplan').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        xyz['s2_masterplan_subplan'] = _id;
        get_masterplan_goal(_id);

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['2'] = xyz['s2_masterplan_subplan'];
        } else {
            xyz_selectbox_secondary['2'] = xyz['s2_masterplan_subplan'];
        }
    }

    $('#row_z_block').hide();
});

$('#select_masterplan_goal').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        xyz['s3_masterplan_goal'] = _id;
        get_masterplan_direction(_id);

        get_value_chain(_id);

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['3'] = xyz['s3_masterplan_goal'];
        } else {
            xyz_selectbox_secondary['3'] = xyz['s3_masterplan_goal'];
        }
    }

    $('#row_z_block').hide();
});

// องค์ประกอบ
$('#select_value_chain').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        xyz['s6_value_chain'] = _id;

        get_factor(_id);

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['6'] = xyz['s6_value_chain'];
        } else {
            xyz_selectbox_secondary['6'] = xyz['s6_value_chain'];
        }
    }

    $('#row_z_block').hide();
});

// ปัจจัย

$('#select_factor').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        xyz['s7_factor'] = _id;

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['7'] = xyz['s7_factor'];
        } else {
            xyz_selectbox_secondary['7'] = xyz['s7_factor'];
        }
    }

    $('#row_z_block').hide();
});

$('#select_masterplan_direction').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        //console.log('s4_masterplan_direction -> ' + _id)
        xyz['s4_masterplan_direction'] = _id;
        get_y2_goal(_id);

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['4'] = xyz['s4_masterplan_direction'];
        } else {
            xyz_selectbox_secondary['4'] = xyz['s4_masterplan_direction'];
        }
    }

    $('#row_z_block').hide();
});

$('#select_y2_goal').change(function () {
    var _id = $(this).val();

    if (_id != '-1') {
        xyz['s5_y2_goal'] = _id;

        get_z_3digits(xyz['s4_masterplan_direction']);

        if (xyz_state == 'direct') {
            xyz_selectbox_direct['5'] = xyz['s5_y2_goal'];
        } else {
            xyz_selectbox_secondary['5'] = xyz['s5_y2_goal'];
        }
    }

    $('#row_z_block').show();
});

$("#btn_masterplan_next_1").on("click", function (e) {
    // console.log('STEP 1: แผนแม่บทภายใต้ยุทธศาสตร์ชาติที่สอดคล้องกับโครงการ');
    // console.log(xyz_template.masterplan + '\t' + get_xyz_text_byID(xyz_template.masterplan));
    // console.log('----------------------------------------------------------------------');

    if (!validate_xyz_step_1()) {
        return;
    }

    xyz_save_tab1();
    xyz_init_tab2();

    next_page_check = 'a_tab_step_2';

    $('#tab_step_2').removeClass('disabled');
    $('#a_tab_step_2').click();

});

$("#btn_masterplan_next_2").on("click", function (e) {
    // console.log('STEP 2.1: ป้าหมายของแผนย่อย Y1');
    // console.log(xyz_template.y1_goal + '\t' + get_xyz_text_byID(xyz_template.y1_goal));
    // console.log('----------------------------------------------------------------------');

    // console.log('STEP 2.2: เป้าหมายของแผนแม่บทประเด็น Y2');
    // console.log(xyz_template.y2_goal + '\t' + get_xyz_text_byID(xyz_template.y2_goal));
    // console.log('----------------------------------------------------------------------');

    next_page_check = 'a_tab_step_3';

    if (!validate_xyz_step_2()) {
        return;
    }

    xyz_save_tab2();
    xyz_init_tab3();

    $('#tab_step_3').removeClass('disabled');
    $('#a_tab_step_3').click();
});

$("#btn_masterplan_next_3").on("click", function (e) {
    // console.log('STEP 3.1: เป้าหมายของยุทธศาสตร์ชาติ');
    // console.log(y1_goal_selected + '\t' + get_xyz_text_byID(y1_goal_selected));
    // console.log('----------------------------------------------------------------------');

    // console.log('STEP 3.2: ประเด็นของยุทธศาสตร์ชาติ ');
    // console.log(y2_goal_selected + '\t' + get_xyz_text_byID(y2_goal_selected));
    // console.log('----------------------------------------------------------------------');

    if (!validate_xyz_step_3()) {
        return;
    }

    next_page_check = 'a_tab_step_4';

    xyz_save_tab3();
    xyz_init_tab4();

    $('#tab_step_4').removeClass('disabled');
    $('#a_tab_step_4').click();
});

$("#btn_masterplan_next_4").on("click", function (e) {

    if (!validate_xyz_step_4()) {
        return;
    }

    xyz_template.report_z_consistent = $('#textbox_z_consistent').val();
    next_page_check = 'a_tab_step_5';

    xyz_save_tab4();
    xyz_init_tab5();

    $('#tab_step_5').removeClass('disabled');
    $('#a_tab_step_5').click();

});

$("#btn_masterplan_back_2").on("click", function (e) {
    next_page_check = 'a_tab_step_1';
    $('#tab_step_1').removeClass('disabled');
    $('#a_tab_step_1').click();
});

$("#btn_masterplan_back_3").on("click", function (e) {
    next_page_check = 'a_tab_step_2';
    $('#tab_step_2').removeClass('disabled');
    $('#a_tab_step_2').click();
});

$("#btn_masterplan_back_4").on("click", function (e) {
    next_page_check = 'a_tab_step_3';
    $('#tab_step_3').removeClass('disabled');
    $('#a_tab_step_3').click();
});

$("#btn_masterplan_back_5").on("click", function (e) {
    next_page_check = 'a_tab_step_4';
    $('#tab_step_4').removeClass('disabled');
    $('#a_tab_step_4').click();
});

/* Save XYZ */

$("#btn_masterplan_save").on("click", function (e) {

    if (xyz_state == 'direct') {
        xyz_template.id = get_xyz_mid(xyz_template);
        xyz_direct = xyz_template;

        $('#xyz_table_primary_topic').text(get_xyz_text_byID(xyz_direct.y1_direction));
        $('#btn_add_xyz_direct').hide();
        $('#xyz_table_primary').show();

        auto_check_tree_strategies_direct([xyz_direct.z_3digits]);
        //auto_check_tree_strategies_direct([xyz_direct.y2_goal]);

        $('#div_info_strategies').hide();
        $('#div_block_strategies_direct').show();
        $('#block_xyz_option').show();

        $('#nav_pills_tab_header_strategies').removeClass('nav_pills_tab_disable');
        $('#nav_pills_tab_header_strategies').attr('data-toggle', 'tab');
        $('#nav_pills_tab_header_strategies').attr('title', '');

    } else {
        xyz_template.id = get_xyz_mid(xyz_template);
        xyz_secondary_arr.push(xyz_template);
        xyz_secondary_arr_temp.push(xyz_template);

        $('#div_info_strategies').hide();
        $('#div_block_strategies_option').show();

        var _row = '<tr><td>';
        _row += '<strong>แนวทางการพัฒนารอง:&nbsp;</strong>' + get_xyz_text_byID(xyz_template.y1_direction);
        _row += '</td><td class="table_td-center">';
        _row += '<a data-toggle="tooltip" title="แสดงข้อมูลแนวทางการพัฒนารอง" onclick="xyz_preview_popup(\'secondary\', \'' + xyz_template.id + '\')" ';
        _row += 'class="btn btn-link btn-lg icon-blocks row-padding-rl-no">';
        _row += '<span class="glyphicon glyphicon-file icon-view"> </span></a>';
        _row += '<a data-toggle="tooltip" id="xyz_secondary_remove_' + xyz_template.id + '" onclick="confirm_remove_xyz(\'secondary\', \'' + xyz_template.id + '\')" title="ลบแนวทางการพัฒนารอง" class="btn btn-link btn-lg icon-blocks row-padding-rl-no">';
        _row += '<span class="glyphicon glyphicon-trash icon-danger"></span></a>';

        $('#xyz_table_secondary > tbody:last-child').append(_row);

        var _temp_z_3digits = [];
        var _temp_z_goal = [];

        for (var i = 0; i < xyz_secondary_arr.length; i++) {
            _temp_z_3digits.push(xyz_secondary_arr[i].z_3digits);
            _temp_z_goal.push(xyz_secondary_arr[i].z_goal);
        }

        auto_check_tree_strategies_secondary(_temp_z_3digits);
        //auto_check_tree_strategies_secondary(_temp_z_goal);

        $('#xyz_table_group_secondary').show();
    }

    $('#main_modal_select_masterplan').modal('hide');

});

/***************** Function for each XYZ's block *************************/

function xyz_reset_form() {
    $('#select_masterplan_title').prop('selectedIndex', 0);
    $('#select_masterplan_subplan').prop('selectedIndex', 0);
    $('#select_masterplan_goal').prop('selectedIndex', 0);
    $('#select_masterplan_direction').prop('selectedIndex', 0);
    $('#select_y2_goal').prop('selectedIndex', 0);

    $('#select_value_chain').prop('selectedIndex', 0);
    $('#select_factor').prop('selectedIndex', 0);

    $("#xyz_error_tree_z_3digits").hide();
    $("#xyz_error_tree_z_target").hide();

    $('#row_z_block').hide();

    $('#textbox_z_consistent').val('');
    $('#textbox_y2_goal_consistent').val('');
    $('#textbox_y1_goal_consistent').val('');

    $('[id^=textbox_y2_goal_contribution_]').val('');
    $('[id^=textbox_y1_goal_contribution_]').val('');

    $('#tab_step_1').removeClass('disabled');
    $('#a_tab_step_1').click();
}

function xyz_init_xyz() {
    xyz_template = {};
    xyz_key = Object.keys(xyz_kv);

    xyz_key.sort();

    xyz_template = {
        "masterplan": "",
        "y1_direction": "",
        "y1_goal": "",
        "y2_goal": "",
        "z_goal": "",
        "z_3digits": "",
        "value_chain": "",
        "factor": "",
        "report_z_consistent": "",
        "report_y1_goal": {
            "consistent": "",
            "indicators": []
        },
        "report_y2_goal": {
            "consistent": "",
            "indicators": []
        }
    };

    json_goalY2_goalZ = load_goalY2_goalZ();
    json_directionY1_Z3digits = load_directionY1_Z3digits();
    json_Z3digits_goalZ = load_Z3digits_goalZ();

    json_value_goldY1_goldY2 = load_value_goalY1_goalY2();

    json_value_chain = load_value_chain();

    tree_z_3digits = $('#z_3digits').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name"
    });

    tree_target_z = $('#plan_target_z').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name",
    });

    xyz_reset_form();

}

function xyz_init_tab2() {
    console.log(xyz_template)
    console.log(json_tree_map_strategies)

    var _html_z_direct = rander_tree_strategic('strategies', [xyz_template.z_3digits], json_tree_map_strategies, 4);
    var _html_z_target = rander_tree_strategic('strategies', [xyz_template.z_goal], json_tree_map_strategies, 4);

    $('#viewer_z_direct').html(_html_z_direct);
    $('#viewer_z_target').html(_html_z_target);
}

function xyz_init_tab3() {
    var y2_goal_interval = get_xyz_nextlink(json_value_goldY1_goldY2, xyz_template.y2_goal);
    var _header_name = get_xyz_text_byID(xyz_template.y2_goal.substr(0, 4));

    $('#viewer_y2_goal_header').text('ความสอดคล้องของโครงการกับเป้าหมายของ' + _header_name);
    $('#viewer_indicator_y2_header').text('ตัวชี้วัดของ' + _header_name);

    if (is_project_idea) {
        $('#viewer_y2_goal').html('<strong>เป้าหมาย:</strong>&nbsp;' + get_xyz_text_byID(xyz_template.y2_goal));
    } else {
        $('#viewer_y2_goal').html('<strong>เป้าหมาย:</strong>&nbsp;' + get_xyz_text_byID(xyz_template.y2_goal) + '<span class="req"> *</span>');
    }

    $('#table_y2_goal').empty();

    for (var i = 0; i < y2_goal_interval.length; i++) {
        var _html = '<label class="control-label control-label-left col-sm-12"><strong>ตัวชี้วัด: </strong>' + get_xyz_text_byID(y2_goal_interval[i].id) + '</label>';

        _html += `
        <div class="col-md-12 row-margin-bottom-20">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-xs-2 text-center">ช่วงปี</th>
                        <th class="col-xs-2 text-center">ปี 2561 - 2565</th>
                        <th class="col-xs-2 text-center">ปี 2566 - 2570</th>
                        <th class="col-xs-2 text-center">ปี 2571 - 2575</th>
                        <th class="col-xs-2 text-center">ปี 2576 - 2580</th>
                    </tr>
                </thead>
                <tr>
                    <td class="text-center">ค่าเป้าหมาย</td>
                    `;

        _html += '<td class="text-center" id="viewer_y2_goal_interval_1">' + y2_goal_interval[i].target[0] + '</td>';
        _html += '<td class="text-center" id="viewer_y2_goal_interval_2">' + y2_goal_interval[i].target[1] + '</td>';
        _html += '<td class="text-center" id="viewer_y2_goal_interval_3">' + y2_goal_interval[i].target[2] + '</td>';
        _html += '<td class="text-center" id="viewer_y2_goal_interval_4">' + y2_goal_interval[i].target[3] + '</td>';

        _html += `</tr>
            </table>
            <div class="input-group">
                <span class="input-group-addon">Contribution ต่อเป้าหมายเมื่อเสร็จสิ้นโครงการ</span>
                `;

        _html += '<input type="text" id="textbox_y2_goal_contribution_' + y2_goal_interval[i].id + '" class="form-control">';
        _html += `
            </div>
        </div>
      `;

        $('#table_y2_goal').append(_html);
    }
}

function xyz_init_tab4() {
    var y1_goal_interval = get_xyz_nextlink(json_value_goldY1_goldY2, xyz_template.y1_goal);

    var _header_name = get_xyz_text_byID(xyz_template.y1_goal.substr(0, 4));

    $('#viewer_y1_goal_header').text('ความสอดคล้องของโครงการกับเป้าหมายของ' + _header_name);
    $('#viewer_indicator_y1_header').text('ตัวชี้วัดของ' + _header_name);

    $('#viewer_y1_goal').html('<strong>เป้าหมาย:</strong>&nbsp;' + get_xyz_text_byID(xyz_template.y1_goal) + '<span class="req"> *</span>');
    $('#table_y1_goal').empty();

    for (var i = 0; i < y1_goal_interval.length; i++) {
        var _html = '<label class="control-label control-label-left col-sm-12"><strong>ตัวชี้วัด: </strong>' + get_xyz_text_byID(y1_goal_interval[i].id) + '</label>';

        _html += `
        <div class="col-md-12 row-margin-bottom-20">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-xs-2 text-center">ช่วงปี</th>
                        <th class="col-xs-2 text-center">ปี 2561 - 2565</th>
                        <th class="col-xs-2 text-center">ปี 2566 - 2570</th>
                        <th class="col-xs-2 text-center">ปี 2571 - 2575</th>
                        <th class="col-xs-2 text-center">ปี 2576 - 2580</th>
                    </tr>
                </thead>
                <tr>
                    <td class="text-center">ค่าเป้าหมาย</td>
                    `;

        _html += '<td class="text-center" id="viewer_y1_goal_interval_1">' + y1_goal_interval[i].target[0] + '</td>';
        _html += '<td class="text-center" id="viewer_y1_goal_interval_2">' + y1_goal_interval[i].target[1] + '</td>';
        _html += '<td class="text-center" id="viewer_y1_goal_interval_3">' + y1_goal_interval[i].target[2] + '</td>';
        _html += '<td class="text-center" id="viewer_y1_goal_interval_4">' + y1_goal_interval[i].target[3] + '</td>';

        if (is_project_idea) {
            _html += `</tr>
            </table>
            <div class="input-group">
                <span class="input-group-addon">ผลสัมฤทธิ์ของโครงการต่อการปิดช่องว่างของเป้าหมาย (project's contribution to target gap)</span>
                `;
        } else {
            _html += `</tr>
            </table>
            <div class="input-group">
                <span class="input-group-addon">Contribution ต่อเป้าหมายเมื่อเสร็จสิ้นโครงการ</span>
                `;
        }

        _html += '<input type="text" id="textbox_y1_goal_contribution_' + y1_goal_interval[i].id + '" class="form-control">';
        _html += `
            </div>
        </div>
      `;

        $('#table_y1_goal').append(_html);
    }
}

function xyz_init_tab5() {
    var _html_z_direct = rander_tree_strategic('strategies', [xyz_template.z_3digits], json_tree_map_strategies, 4);
    var _html_z_target = rander_tree_strategic('strategies', [xyz_template.z_goal], json_tree_map_strategies, 4);

    let _text_value_chain = get_value_chain_text_byID(xyz_template.value_chain, xyz_template.factor);

    //console.log(xyz_template);

    $('#viewer_preview_z_direct').html(_html_z_direct);
    $('#viewer_preview_z_target').html(_html_z_target);
    $('#viewer_preview_z_consistent').text(xyz_template.report_z_consistent);

    $('#viewer_preview_y2_header').text(get_xyz_text_byID(xyz_template.y2_goal.substr(0, 4)));
    $('#viewer_preview_y2_goal').text(get_xyz_text_byID(xyz_template.y2_goal));
    $('#viewer_preview_y2_goal_consistent').text(xyz_template.report_y2_goal.consistent);
    $('#viewer_preview_y2_goal_indicators').html(create_table_y_indicator(xyz_template.report_y2_goal.indicators));

    $('#viewer_preview_y1_header').text(get_xyz_text_byID(xyz_template.y1_goal.substr(0, 4)));
    $('#viewer_preview_masterplan').text(get_xyz_text_byID(xyz_template.y1_direction));
    $('#viewer_preview_y1_goal').text(get_xyz_text_byID(xyz_template.y1_goal));
    $('#viewer_preview_y1_goal_consistent').text(xyz_template.report_y1_goal.consistent);
    $('#viewer_preview_y1_goal_indicators').html(create_table_y_indicator(xyz_template.report_y1_goal.indicators));

    $('#preview_viewer_preview_value_chain').text(_text_value_chain.value_chain);
    $('#preview_viewer_preview_factor').text(_text_value_chain.factor);

}

function xyz_preview_popup(_type, _xyz) {
    var _y2_header = null;

    if (_type == 'direct') {
        _y2_header = _xyz.y2_goal.substr(0, 2) + '00';
        $('#preview_sub_viewer_header').text('แนวทางการพัฒนาหลัก: ' + get_xyz_text_byID(_y2_header));

    } else {
        _xyz = xyz_secondary_arr_temp[get_array_index_bykey(xyz_secondary_arr_temp, _xyz)];

        _y2_header = _xyz.y2_goal.substr(0, 2) + '00';
        $('#preview_sub_viewer_header').text('แนวทางการพัฒนารอง: ' + get_xyz_text_byID(_y2_header));

    }

    var _html_z_direct = rander_tree_strategic('strategies', [_xyz.z_3digits], json_tree_map_strategies, 4);
    var _html_z_target = rander_tree_strategic('strategies', [_xyz.z_goal], json_tree_map_strategies, 4);

    let _text_value_chain = get_value_chain_text_byID(_xyz.value_chain, _xyz.factor);

    $('#preview_sub_viewer_preview_z_direct').html(_html_z_direct);
    $('#preview_sub_viewer_preview_z_target').html(_html_z_target);
    $('#preview_sub_viewer_preview_z_consistent').text(_xyz.report_z_consistent);

    $('#preview_sub_viewer_preview_y2_header').text(get_xyz_text_byID(_y2_header));
    $('#preview_sub_viewer_preview_y2_goal').text(get_xyz_text_byID(_xyz.y2_goal));
    $('#preview_sub_viewer_preview_y2_goal_consistent').text(_xyz.report_y2_goal.consistent);
    $('#preview_sub_viewer_preview_y2_goal_indicators').html(create_table_y_indicator(_xyz.report_y2_goal.indicators));

    $('#preview_sub_viewer_preview_y1_header').text(get_xyz_text_byID(_xyz.y1_goal.substr(0, 4)));
    $('#preview_sub_viewer_preview_masterplan').text(get_xyz_text_byID(_xyz.y1_direction));
    $('#preview_sub_viewer_preview_y1_goal').text(get_xyz_text_byID(_xyz.y1_goal));
    $('#preview_sub_viewer_preview_y1_goal_consistent').text(_xyz.report_y1_goal.consistent);
    $('#preview_sub_viewer_preview_y1_goal_indicators').html(create_table_y_indicator(_xyz.report_y1_goal.indicators));

    $('#preview_sub_viewer_preview_value_chain').text(_text_value_chain.value_chain);
    $('#preview_sub_viewer_preview_factor').text(_text_value_chain.factor);

    $('#main_modal_preview_masterplan').modal('show');
}

/***************** Function for save  XYZ's data *************************/

function xyz_save_tab1() {
    xyz_template.masterplan = xyz['s2_masterplan_subplan'];
    xyz_template.y1_goal = xyz['s3_masterplan_goal'];
    xyz_template.y1_direction = xyz['s4_masterplan_direction'];
    xyz_template.y2_goal = xyz['s5_y2_goal'];
    xyz_template.z_3digits = xyz['z_3digits'];
    xyz_template.z_goal = xyz['z_goal_z'];
    xyz_template.value_chain = xyz['s6_value_chain'];
    xyz_template.factor = xyz['s7_factor'];
}

function xyz_save_tab2() {
    xyz_template.report_z_consistent = $('#textbox_z_consistent').val();
}

function xyz_save_tab3() {
    xyz_template.report_y2_goal.consistent = $('#textbox_y2_goal_consistent').val();

    var _temp_y2_indicators = [];

    $('[id^=textbox_y2_goal_contribution_]').each(function () {
        _temp_y2_indicators.push({
            "id": $(this).attr('id').split('_')[4],
            "contribution": $(this).val()
        });
    });

    xyz_template.report_y2_goal.indicators = _temp_y2_indicators;
}

function xyz_save_tab4() {
    xyz_template.report_y1_goal.consistent = $('#textbox_y1_goal_consistent').val();

    var _temp_y1_indicators = [];

    $('[id^=textbox_y1_goal_contribution_]').each(function () {
        _temp_y1_indicators.push({
            "id": $(this).attr('id').split('_')[4],
            "contribution": $(this).val()
        });
    });

    xyz_template.report_y1_goal.indicators = _temp_y1_indicators;
}

/******************************************************************/

function get_masterplan_subplab(_id) {
    //console.log('(2) ชื่อแผนย่อย');
    reset_select_masterplan_subplan();

    xyz_key.forEach(element => {
        if (element.length == 4) {
            if (element.substring(0, 2) == _id && element.substring(2, 4) != '00') {
                //console.log(element + ' ' + xyz_kv[element])
                $('#select_masterplan_subplan')
                    .append($("<option></option>")
                        .attr("value", element)
                        .attr("title", xyz_kv[element])
                        .text(xyz_kv[element]));
            }
        }

    });
    //console.log('-----------------------------');
}

function get_masterplan_goal(_id) {
    //console.log('(3) เป้าหมายแผนย่อย');
    reset_select_masterplan_goal();

    xyz_key.forEach(element => {
        if (element.length == 6) {
            if (element.substring(0, 4) == _id) {
                //console.log(element + ' ' + xyz_kv[element])
                $('#select_masterplan_goal')
                    .append($("<option></option>")
                        .attr("value", element)
                        .attr("title", xyz_kv[element])
                        .text(xyz_kv[element]));
            }
        }
    });
    //console.log('-----------------------------');
}

function get_masterplan_direction(_id) {
    //console.log('(4) แนวทางการพัฒนาภายใต้แผนย่อย');
    reset_select_masterplan_direction();

    xyz_key.forEach(element => {
        if (element.substring(0, 4) == _id.substring(0, 4)) {
            if (element.substring(4, 6) == '00') {
                //console.log(element + ' ' + xyz_kv[element])

                var _text = transform_text_style(xyz_kv[element]);

                if (check_disable_selectbox(xyz_kv[element])) {
                    $('#select_masterplan_direction')
                        .append($("<option></option>")
                            .attr("disabled", "disabled")
                            .attr("value", element)
                            .attr("title", _text)
                            .text(_text));
                } else {
                    $('#select_masterplan_direction')
                        .append($("<option></option>")
                            .attr("value", element)
                            .attr("title", _text)
                            .text(_text));
                }
            }
        }
    });
}

// องค์ประกอบ
function get_value_chain(_id) {
    reset_vlue_chain();

    let value_chain_filter = json_value_chain.filter(function (el) {
        return el.masterplan_goal_id == _id;
    });

    let temp_value_chain = [];

    value_chain_filter.forEach(element => {
        if (!temp_value_chain.includes(element.value_chain_id)) {
            $('#select_value_chain')
                .append($("<option></option>")
                    .attr("value", element.value_chain_id)
                    .attr("title", element.value_chain_text)
                    .text(element.value_chain_text));

            temp_value_chain.push(element.value_chain_id);
        }

    });
}

// ปัจจัย
function get_factor(_id) {
    reset_factor();
    
    let factor_filter = json_value_chain.filter(function (el) {
        return el.value_chain_id == _id;
    });

    let temp_factor = [];

    factor_filter.forEach(element => {
        if (!temp_factor.includes(element.factor_id)) {
            $('#select_factor')
                .append($("<option></option>")
                    .attr("value", element.factor_id)
                    .attr("title", element.factor_text)
                    .text(element.factor_text));

            temp_factor.push(element.factor_text);
        }

    });
}

function get_y2_goal(_id) {
    //console.log('(5) เป้าหมายของแผนแม่บทประเด็น');
    reset_select_y2_goal();

    xyz_key.forEach(element => {
        if (element.length == 6) {
            if (element.substring(0, 2) == xyz['s1_masterplan_title']) {
                if (element.substring(2, 4) == '00') {
                    //console.log(element + ' ' + xyz_kv[element])
                    $('#select_y2_goal')
                        .append($("<option></option>")
                            .attr("value", element)
                            .attr("title", xyz_kv[element])
                            .text(xyz_kv[element]));
                }
            }
        }
    });
    //console.log('-----------------------------');

}

function get_z_3digits(_s4) {

    tree_target_z = $('#plan_target_z').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name",
    });

    /*
    tree_plan_z = $('#plan_z').tree({
        primaryKey: 'id',
        checkboxes: false,
        selectionType: 'single',
        uiLibrary: 'bootstrap',
        textField: "name"
    });
    */

    y1_direction_z3digit = get_xyz_nextlink(json_directionY1_Z3digits, _s4);

    //console.log('(6) + (7) ยุทธศาสตร์ชาติ');

    for (var x = 0; x < y1_direction_z3digit.length; x++) {
        //console.log(y1_direction_z3digit[x] + ' ' + tree_get_name_strategies(json_tree_map_strategies, y1_direction_z3digit[x])[0]);
    }
    //console.log('-----------------------------');

    build_tree_z(tree_z_3digits, y1_direction_z3digit);

    tree_z_3digits.on('select', function (e, node, z_3digits_id) {
        if (z_3digits_id.length < 10) {
            return false;
        }

        // Get goal Z from goal Y2.
        goal_z_from_goalY2 = get_xyz_nextlink(json_goalY2_goalZ, xyz['s5_y2_goal']);

        // Get goal Z from Z 3-digits.
        xyz['z_3digits'] = z_3digits_id;
        goal_z_from_z3digit = get_xyz_nextlink(json_Z3digits_goalZ, z_3digits_id);

        goal_z_intersection = intersect(goal_z_from_goalY2, goal_z_from_z3digit);

        tree_target_z.reload();
        build_tree_z_targer(tree_target_z, goal_z_intersection);

        tree_target_z.on('select', function (e, node, goal_z_id) {
            if (goal_z_id.length != 8) {
                return false;
            }
            xyz['z_goal_z'] = goal_z_id;
        }); // goal

        // console.log('goal Y2 -> goal Z: ' + goal_z_from_goalY2.join(', '));
        // console.log('Z 3-digits -> goal Z: ' + goal_z_from_z3digit.join(', '));
        // console.log('intersection: ' + goal_z_intersection.join(', '));
        // console.log('----------------------------------------------------------------------');
    });

}

/***************** Get tree data *************************/

function get_xyz_nextlink(_obj, _current_id) {
    if (_obj[_current_id] === undefined) {
        return -1;
    } else {
        return _obj[_current_id];
    }
}

function get_xyz_text_byID(_id) {
    if (xyz_kv[_id] === undefined) {
        return -1;
    } else {
        return xyz_kv[_id];
    }
}

function get_value_chain_text_byID(_value_chain, _factor) {
    let value_chain = {
        value_chain: '',
        factor: ''
    };

    if (typeof _value_chain != 'undefined' || typeof _factor != 'undefined') {
        let value_chain_filter = json_value_chain.filter(function (el) {
            return el.value_chain_id == _value_chain && el.factor_id == _factor;
        });

        if (value_chain_filter.length > 0) {
            value_chain.value_chain = value_chain_filter[0].value_chain_text;
            value_chain.factor = value_chain_filter[0].factor_text;
        }
    }

    return value_chain;
}

function build_tree_z(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {

        if (typeof _tree.getNodeById(_left_node[i]) == 'undefined') {

            var _z_3d_id = _left_node[i].substr(0, 4);
            var _z_mid_id = _left_node[i].substr(0, 8);

            var _id = _left_node[i];
            var _name = tree_get_name_strategies(json_tree_map_strategies, _left_node[i].replace((
                /  |\r\n|\n|\r/gm), ""));

            if (typeof _tree.getNodeById(_z_3d_id) == 'undefined') {
                _tree.addNode({
                    id: _z_3d_id,
                    name: '<strong>' + tree_get_name_strategies(json_tree_map_strategies, _z_3d_id) + '<strong>'
                });

            }

            if (typeof _tree.getNodeById(_z_mid_id) == 'undefined') {
                var _root = _tree.getNodeById(_z_3d_id);

                _tree.addNode({
                    id: _z_mid_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_mid_id)
                }, _root);
            }

            var _parent = _tree.getNodeById(_z_mid_id);

            _tree.addNode({
                id: _id,
                name: _name,
                "imageCssClass": "glyphicon glyphicon-chevron-right"
            }, _parent);
        }
    }
}

function build_tree_z_targer(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {

        if (typeof _tree.getNodeById(_left_node[i]) == 'undefined') {
            var _z_3d_id = _left_node[i].substr(0, 4);
            var _id = _left_node[i];
            var _name = tree_get_name_strategies(json_tree_map_strategies, _left_node[i].replace((
                /  |\r\n|\n|\r/gm), ""));

            if (typeof _tree.getNodeById(_z_3d_id) == 'undefined') {

                //var _parent = _tree.getNodeById(_z_3d_id);
                _tree.addNode({
                    id: _z_3d_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_3d_id)
                });

            }

            var _parent = _tree.getNodeById(_z_3d_id);
            _tree.addNode({
                id: _id,
                name: _name,
                "imageCssClass": "glyphicon glyphicon-chevron-right"
            }, _parent);
        }
    }
}

function intersect(a, b) {
    if (a == -1 || b == -1) {
        return [];
    }

    var setA = new Set(a);
    var setB = new Set(b);
    var intersection = new Set([...setA].filter(x => setB.has(x)));
    return Array.from(intersection);
}

/***************** Load json data *************************/

function load_goalY2_goalZ() {
    var _read_data = JSON.parse($.getJSON({
        'url': "assets/treeview_jsons/v2/xyz/3-goalY2-goalZ.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_directionY1_Z3digits() {
    var _read_data = JSON.parse($.getJSON({
        'url': "assets/treeview_jsons/v2/xyz/5-directionY1-Z3digits.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_Z3digits_goalZ() {
    var _read_data = JSON.parse($.getJSON({
        'url': "assets/treeview_jsons/v2/xyz/6-Z3digits-goalZ.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_value_goalY1_goalY2() {
    var _read_data = JSON.parse($.getJSON({
        'url': "assets/treeview_jsons/v2/xyz/value-goalY1-goalY2.json",
        'async': false
    }).responseText);

    return _read_data;
}

function load_value_chain() {
    var _read_data = JSON.parse($.getJSON({
        'url': "assets/treeview_jsons/v2/xyz/8-value_chain.json",
        'async': false
    }).responseText);

    return _read_data;
}

/***************** Tree helper function *************************/
function build_tree_goal(_tree, _left_node) {

    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {
        _tree.addNode({
            id: _left_node[i],
            name: get_xyz_text_byID(_left_node[i]),
            "imageCssClass": "glyphicon glyphicon-chevron-right"
        });
    }
}

function build_tree_goal_header(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {
        var _y_4d_id = _left_node[i].substr(0, 4);

        if (typeof _tree.getNodeById(_y_4d_id) == 'undefined') {
            _tree.addNode({
                id: _y_4d_id,
                name: get_xyz_text_byID(_y_4d_id)
            });
        }
        _parent = _tree.getNodeById(_y_4d_id);
        _tree.addNode({
            id: _left_node[i],
            name: get_xyz_text_byID(_left_node[i]),
            "imageCssClass": "glyphicon glyphicon-chevron-right"
        }, _parent);
    }
}

function build_tree_z_targer(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {

        if (typeof _tree.getNodeById(_left_node[i]) == 'undefined') {
            var _z_3d_id = _left_node[i].substr(0, 4);
            var _id = _left_node[i];
            var _name = tree_get_name_strategies(json_tree_map_strategies, _left_node[i]);

            if (typeof _tree.getNodeById(_z_3d_id) == 'undefined') {

                var _parent = _tree.getNodeById(_z_3d_id);
                _tree.addNode({
                    id: _z_3d_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_3d_id)
                }, _parent);

            }

            var _parent = _tree.getNodeById(_z_3d_id);
            _tree.addNode({
                id: _id,
                name: _name,
                "imageCssClass": "glyphicon glyphicon-chevron-right"
            }, _parent);
        }
    }
}

function build_tree_z_intersection(_tree, _left_node) {
    if (_left_node == -1) {
        return;
    }

    for (var i = 0; i < _left_node.length; i++) {

        if (typeof _tree.getNodeById(_left_node[i]) == 'undefined') {

            var _z_3d_id = _left_node[i].substr(0, 4);
            var _z_mid_id = _left_node[i].substr(0, 8);

            var _id = _left_node[i];
            var _name = tree_get_name_strategies(json_tree_map_strategies, _left_node[i]);

            if (typeof _tree.getNodeById(_z_3d_id) == 'undefined') {
                _tree.addNode({
                    id: _z_3d_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_3d_id)
                }, _root);

            }

            if (typeof _tree.getNodeById(_z_mid_id) == 'undefined') {
                var _root = _tree.getNodeById(_z_3d_id);

                _tree.addNode({
                    id: _z_mid_id,
                    name: tree_get_name_strategies(json_tree_map_strategies, _z_mid_id)
                }, _root);
            }

            var _parent = _tree.getNodeById(_z_mid_id);

            _tree.addNode({
                id: _id,
                name: _name,
                "imageCssClass": "glyphicon glyphicon-chevron-right"
            }, _parent);
        }
    }
}

function get_xyz_nextlink(_obj, _current_id) {
    if (_obj[_current_id] === undefined) {
        return -1;
    } else {
        return _obj[_current_id];
    }
}

function get_xyz_text_byID(_id) {
    if (xyz_kv[_id] === undefined) {
        return -1;
    } else {
        return xyz_kv[_id];
    }
}

/***************** Element helper function *****************/
function transform_text_style(_text) {
    var map_keyword = {
        '<disabled>': '',
        '<tab>': '\xA0\xA0\xA0\xA0\xA0\xA0',
    };

    var re = new RegExp(Object.keys(map_keyword).join("|"), "gi");
    _text = _text.replace(re, function (matched) {
        return map_keyword[matched.toLowerCase()];
    });

    return _text;
}

function check_disable_selectbox(_text) {
    if (_text.search('<disabled>') >= 0) {
        return true;
    } else {
        return false;
    }
}

/***************** Reset view *************************/
function reset_select_masterplan_subplan() {
    $('#select_masterplan_subplan').empty();
    $('#select_masterplan_subplan')
        .append($("<option></option>")
            .attr("value", '-1')
            .text('เลือกชื่อแผนย่อย'));

    reset_select_masterplan_goal();
}

function reset_select_masterplan_goal() {
    $('#select_masterplan_goal').empty();
    $('#select_masterplan_goal')
        .append($("<option ></option>")
            .attr("value", '-1')
            .text('เลือกเป้าหมายแผนย่อย'));

    reset_select_masterplan_direction();
    reset_vlue_chain();
}

function reset_select_masterplan_direction() {
    $('#select_masterplan_direction').empty();
    $('#select_masterplan_direction')
        .append($("<option></option>")
            .attr("value", '-1')
            .text('เลือกแนวทางพัฒนาแผนย่อย'));

    reset_select_y2_goal();
}

function reset_select_y2_goal() {
    $('#select_y2_goal').empty();
    $('#select_y2_goal')
        .append($("<option></option>")
            .attr("value", '-1')
            .text('เลือกเป้าหมายของแผนแม่บทประเด็น'));

    reset_tree();
}

function reset_tree() {
    tree_z_3digits.reload();
    tree_target_z.reload();
}

function reset_vlue_chain() {
    $('#select_value_chain').empty();
    $('#select_value_chain')
        .append($("<option></option>")
            .attr("value", '-1')
            .text('เลือกองค์ประกอบ'));

    reset_factor();
}

function reset_factor() {
    $('#select_factor').empty();
    $('#select_factor')
        .append($("<option></option>")
            .attr("value", '-1')
            .text('เลือกปัจจัย'));
}

function create_table_y_indicator(_indicators) {
    if (_indicators.length > 0) {
        var _temp_text = '<table class="table table-condensed"><thead><tr><th>ตัวชี้วัด</th><th>Contribution ต่อเป้าหมายเมื่อเสร็จสิ้นโครงการ</th></tr></thead><tbody>';

        for (var i = 0; i < _indicators.length; i++) {

            _temp_text += '<tr><td>';
            _temp_text += get_xyz_text_byID(_indicators[i].id);
            _temp_text += '</td><td>';
            _temp_text += _indicators[i].contribution;
            _temp_text += '</td></tr>';
        }
        _temp_text += '</tbody></table>';

        return _temp_text;

    } else {
        return 'ไม่มีข้อมูลตัวชี้วัด';
    }
}

function confirm_remove_xyz(_type, _id) {
    if (_type == 'direct') {
        xyz_state_remove = 'direct';

        $('#xyz_confirm_delete_header').text('ยืนยันการลบข้อมูลแนวทางการพัฒนาหลัก');
        $('#xyz_confirm_delete_message').text(get_xyz_text_byID(xyz_direct.masterplan));
    } else {
        xyz_state_remove = 'secondary';

        $('#xyz_confirm_delete_header').text('ยืนยันการลบข้อมูลแนวทางการพัฒนารอง');
        $('#xyz_confirm_delete_message').text(get_xyz_text_byID(xyz_secondary.masterplan));
    }

    $('#xyz_confirm_delete').modal('show');
}

function intersect(a, b) {
    if (a == -1 || b == -1) {
        return [];
    }

    var setA = new Set(a);
    var setB = new Set(b);
    var intersection = new Set([...setA].filter(x => setB.has(x)));
    return Array.from(intersection);
}

function get_array_index_bykey(_arr, _key) {
    return _arr.map(x => {
        return x.id;
    }).indexOf(_key);
}

function get_xyz_selectbox(_xyz) {
    return _xyz.masterplan + _xyz.y1_direction + _xyz.y1_goal + _xyz.y2_goal;
}

function get_xyz_mid(_xyz) {
    return _xyz.masterplan + _xyz.y1_direction + _xyz.y1_goal + _xyz.y2_goal + _xyz.z_goal + _xyz.z_3digits;

}