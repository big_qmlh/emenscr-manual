$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});

function document_ready_load() {
    inti_treeview_all();

    let params = get_url_parameters(window.location.href);

    console.log(params);

    document_id = params.id;

    if (typeof document_id == 'undefined' || document_id == '') {
        alert('กรุณาระบุ _id ของโครงการ');

        return;
    }

    console.log(document_id);
}

function load_form_data() {
    load_document(document_id);
}

$(document).ready(function () {
    /******************* On page start *******************/
    init_alert_on_page_start_tab_1();

    /******************* On click Next button *******************/

    $("#btn_control_next_tab_1").on("click", function (e) {

        if (!validate_tab1_all() && ($('#tab1_part1_textbox_project_name').val() != '1')) {
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');

            return;
        }

        $('.panel-collapse.in').collapse('hide');
        $('[id^="nav_pills_tab_planreform_tab_header_"]').removeClass('active');

        get_data_tab_1();
        $('#tab_glyphicon_ok_1').show();

        emenscr_submit_confirn();
    });

    $("#btn_modal_save_project").on("click", function (e) {

        if ($('#btn_modal_save_project').attr('disabled') == 'disabled') {
            return;
        }

        $('#btn_modal_save_project').attr('disabled', true);

        emenscr_update_full_form(document_id);

    });

    /******************* On click Review button *******************/

    $('#form_main_preview_modal').on('shown.bs.modal', function (event) {

        $('#form_main_preview_modal').scrollTop(0);

        $(this).animate({
            scrollTop: ($(section_offset_id_select).offset().top - section_offset_top_before) - 30
        }, 'slow');

        if (section_offset_id_select == '#section4' || section_offset_id_select == '#section5' || section_offset_id_select == '#section6') {
            form_review_rander_tab_4_gantt_modal();
        }
    });

    $('#form_main_preview_modal').on('hidden.bs.modal', function (event) {
        if (section_offset_id_select == '#section4' || section_offset_id_select == '#section5' || section_offset_id_select == '#section6') {
            init_gantt($("#tab3_part5_datetimepicker_project_start").val(), $("#tab3_part5_datetimepicker_project_end").val());
            chenge_gantt_date($("#tab3_part5_datetimepicker_project_start").val(), $("#tab3_part5_datetimepicker_project_end").val());
        }
    });

    $("#btn_control_preview_tab_1").on("click", function (e) {
        section_offset_top_before = $('#section1').offset().top;
        section_offset_id_select = '#section1';

        form_review_rander_tab_1();
        $('#form_main_preview_modal').modal('show');

        return false;
    });

});

function init_alert_on_page_start_project_idea() {
    $('#nav_pills_tab_planreform_tab_header_1 > a').addClass("nav_pills_tab_disable");
    $('#nav_pills_tab_planreform_tab_header_2 > a').addClass("nav_pills_tab_disable");
    $('#nav_pills_tab_planreform_tab_header_3 > a').addClass("nav_pills_tab_disable");
    $('#nav_pills_tab_header_government_ministers').addClass("nav_pills_tab_disable");

    $('#nav_pills_tab_planreform_tab_header_1').click(false);
    $('#nav_pills_tab_planreform_tab_header_2').click(false);
    $('#nav_pills_tab_planreform_tab_header_3').click(false);
    $('#nav_pills_tab_header_government_ministers').click(false);

    $('.project_idea_element').hide();
}

function init_alert_on_page_start_tab_1() {
    /******************* On page start *******************/
    $("#alert_tab1_treeview_no_strategies_direct").hide();
    $("#alert_tab1_treeview_no_level_2").hide();

    $("#alert_tab1_treeview_strategies_direct").hide();
    $("#alert_tab1_treeview_strategies_option").hide();
    $("#alert_tab1_treeview_reform_direct").hide();
    $("#alert_tab1_treeview_reform_option").hide();
    $("#alert_tab1_treeview_plan12").hide();
    $("#alert_tab1_treeview_homeland_policy").hide();

    $("#alert_tab1_government_policy_main").hide();

    $("#space_planreform_main_strategies_loading").hide();
    $("#space_planreform_main_loading").hide();
    $('#alert_tab5_part6_plan_budget_sum').hide();
    $('#alert_tab6_part1_progress_gantt_activity').hide();

    $('span[id^="tab_glyphicon_ok_"]').hide();

    $('#div_info_strategies').show();
    $('#div_block_strategies_direct').hide();
    $('#div_block_strategies_option').hide();

    $('#block_xyz_option').hide();
    $('#block_reform_option').hide();

    $('#nav_pills_tab_header_strategies').addClass('nav_pills_tab_disable');
    $('#nav_pills_tab_header_strategies').attr('data-toggle', 'tooltip');
    $('#nav_pills_tab_header_strategies').attr('title', 'ยุทธศาสตร์ชาติจะถูกเชื่อมโยงผ่านแผนแม่บทภายใต้ยุทธศาสตร์ชาติให้โดยอัตโนมัติ กรุณาเลือกแผนแม่บทภายใต้ยุทธศาสตร์ชาติด้านล่าง');

}

$(document).on('click', '#div_dropdown_treeview_strategies_planreform .dropdown-menu li', function (event) {
    $('#btn_treeview_trategies_planreform b').remove().appendTo($('#btn_treeview_trategies_planreform').text($(this).text()));
});


$(document).on('click', '#div_dropdown_treeview_planreform .dropdown-menu li', function (event) {
    $('#btn_treeview_planreform b').remove().appendTo($('#btn_treeview_planreform').text($(this).text()));
});

// Tab1 part 1: เป้าหมายรวมของยุทธศาสตร์ชาติ
(function ($) {
    $(function () {

        var addFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();

            $(this)
                .toggleClass('btn-success btn-add btn-danger btn-remove')
                .html('<span class="glyphicon glyphicon-minus"></span>&nbsp;&nbsp;ลบ');

            $formGroupClone.find('input').val('');
            $formGroupClone.find('.concept').text('Phone');
            $formGroupClone.insertAfter($formGroup);

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };

        var removeFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }

            $formGroup.remove();
        };

        var selectFormGroup = function (event) {
            event.preventDefault();

            var $selectGroup = $(this).closest('.input-group-select');
            var param = $(this).attr("href").replace("#", "");
            var concept = $(this).text();

            $selectGroup.find('.concept').text(concept);
            $selectGroup.find('.input-group-select-val').val(param);

        };

        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };

        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
        $(document).on('click', '.dropdown-menu a', selectFormGroup);

    });
})(jQuery);

function emenscr_submit_confirn() {
    $('#main_modal_submit_confirm_project_name').text($('#tab1_part1_textbox_project_name').val());
    $('#main_modal_submit_confirm').modal('show');
}

function emenscr_update_full_form(_id) {
    keycloak.updateToken();

    const token = get_keycloak_token();
    const full_form_json = form_full_save_data();
    const url = API_DOCUMENT_ENDPOINT + _id;

    const _headers = {
        'Content-Type': 'application/json; charset=utf-8',
        apikey: keycloak.tokenParsed.apikey,
        Authorization: token,
    }

    return axios.put(url,
            full_form_json, {
                headers: _headers
            })
        .then(function (response) {
            console.log(response);

            /*
            if (json_full_source.document.status == 'เอกสารผ่านการอนุมัติแล้ว') {
                console.log('[log] Created and masterplan are true');
                console.log(json_full_source)

                let workflow_start_url = API_DOCUMENT_ENDPOINT + _id + '/workflow/main_process/start';
                let approver_upper = json_full_source.document.approvers[0].username;
                let _data = {
                    workflow: approver_upper
                };

                let _wf_header = {
                    apikey: keycloak.tokenParsed.apikey,
                    Authorization: _token,
                };

                return axios.post(workflow_start_url,
                        _data, {
                            headers: _wf_header
                        })
                    .then(function (response) {
                        console.log(response);

                        if (response.status == 200) {
                            $('#main_modal_submit_result_project_name').text($('#tab1_part1_textbox_project_name').val());
                            $('#main_modal_submit_result_response_id').text(document_code_th);
                            $('#main_modal_submit_result').modal({
                                backdrop: 'static',
                                keyboard: true,
                                show: true
                            });

                            return;
                        } else {
                            alert("บันทึกโครงการไม้สำเร็จ กรุณาเข้าใช้งานภายหลัง " + JSON.stringify(error.response.data));
                            $('#btn_modal_save_project').attr('disabled', false);

                            return;
                        }
                    })
                    .catch(function (error) {
                        console.log('[error/create] Error on submit document');
                        console.log(error);
                        console.log(error.response)

                        alert("ไม่สามารถบันทึกข้อมูลโครงการได้ กรุณาเข้าใช้งานภายหลัง " + JSON.stringify(error.response.data));
                        $('#btn_modal_save_project').attr('disabled', false);
                        return;
                    });
            }
            */

            if (response.status == 200 && response.data.ok == 1) {
                $('#main_modal_submit_result_project_name').text($('#tab1_part1_textbox_project_name').val());
                $('#main_modal_submit_result_response_id').text(document_code_th);
                $('#main_modal_submit_result').modal({
                    backdrop: 'static',
                    keyboard: true,
                    show: true
                });

                return;
            } else {
                alert("บันทึกโครงการไม้สำเร็จ กรุณาเข้าใช้งานภายหลัง " + JSON.stringify(error.response.data));
                $('#btn_modal_save_project').attr('disabled', false);

                return;
            }
        })
        .catch(function (error) {
            console.log('[error/create] Error on submit document');
            console.log(error);
            console.log(error.response)

            alert("ไม่สามารถบันทึกข้อมูลโครงการได้ กรุณาเข้าใช้งานภายหลัง " + JSON.stringify(error.response.data));
            $('#btn_modal_save_project').attr('disabled', false);
            return;
        });
}